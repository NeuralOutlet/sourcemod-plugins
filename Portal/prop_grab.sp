#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>
#include <float>

int g_grabbedEntity[MAXPLAYERS+1];
int g_lastButtons[MAXPLAYERS+1];
int g_holdTimer[MAXPLAYERS+1];
bool g_dropping[MAXPLAYERS+1];

public Plugin myinfo = 
{
	name = "Prop Grab",
	author = "iBreatheTea",
	description = "A simple mod for picking up the various Portal objects in the ptc mod.",
	version = "1.0",
	url = ""
};

public OnPluginStart() 
{
	HookEvent("player_spawn", PG_PlayerSpawn);
}

public Action PG_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	g_grabbedEntity[client] = -1;
	g_holdTimer[client] = -1;
	g_dropping[client] = false;
}

public OnEntityCreated(entity, const String:classname[])
{
	SDKHook(entity, SDKHook_Use, OnUseHook);
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if (!IsValidClient(client))
		return Plugin_Continue;

	if ((buttons & IN_USE))
	{
		if (!(g_lastButtons[client] & IN_USE))
		{
			// drop the old entity
			if (g_grabbedEntity[client] != -1)
			{
				int entity = g_grabbedEntity[client];
				
				// Give the entity a full collision group
				SetEntProp(entity, Prop_Data, "m_CollisionGroup", 0);
				AcceptEntityInput(entity, "Wake"); // don't be frozen in the air

				g_grabbedEntity[client] = -1;
				g_dropping[client] = true;
				
				float clientPos[3], holdingPos[3], clientVel[3];
				GetEntPropVector(client, Prop_Data, "m_vecOrigin", clientPos);
				GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", clientVel);
				
				GetHoldPosition(client, holdingPos);
				TeleportEntity(entity, holdingPos, NULL_VECTOR, clientVel);
				
				SetEntityRenderMode(entity, RENDER_NORMAL);
				
				// Kill the grab timer and reset control values
				if (g_holdTimer[client] != -1)
				{
					KillTimer(g_holdTimer[client]);
					g_holdTimer[client] = -1;
				}
			}
		}
	}

	g_lastButtons[client] = buttons;
	
	return Plugin_Continue;
}

public Action:OnUseHook(entity, client, caller, UseType:type, Float:value)
{
	// prepare the new entity to hold
	new String:name[64];
	GetEntityClassname(entity,name, sizeof(name));
	
	if(StrEqual("prop_physics", name) || StrEqual("prop_dynamic", name))
    {
		// picking it up
		if (g_grabbedEntity[client] == -1)
		{
			if (!g_dropping[client])
			{
				// make it so the cube no longer interacts with the player
				SetEntProp(entity, Prop_Data, "m_CollisionGroup", 2);
				g_grabbedEntity[client] = entity;
				
				SetEntityRenderMode(entity, RENDER_TRANSALPHA);
				SetEntityRenderColor(entity, _, _, _, 128);
				
				DataPack pack;
				g_holdTimer[client] = CreateDataTimer(0.01, Timer_UpdateHold, pack, TIMER_REPEAT);
				pack.WriteCell(client);
				pack.WriteCell(entity);
			}
			else
			{
				g_dropping[client] = false;
			}
		}
	}
	
	return Plugin_Continue;
}

public Action Timer_UpdateHold(Handle timer, DataPack pack)
{
	pack.Reset();
	int client = pack.ReadCell();
	int entity = pack.ReadCell();
	
	if (!IsValidClient(client) || entity == -1)
		return Plugin_Continue;
	
	float clientPos[3], holdingPos[3];
	GetEntPropVector(client, Prop_Data, "m_vecOrigin", clientPos);
	
	GetHoldPosition(client, holdingPos);
	TeleportEntity(entity, holdingPos, NULL_VECTOR, NULL_VECTOR);
	
	return Plugin_Continue;
}

void GetHoldPosition(int client, float output[3])
{
	float startpos[3], angle[3];
	float fwdDirection[3];

	GetClientEyeAngles(client,angle);
	GetClientEyePosition(client,startpos);
	GetAngleVectors(angle, fwdDirection, NULL_VECTOR, NULL_VECTOR);
	
	float x = 60.0 * fwdDirection[0];
	float y = 60.0 * fwdDirection[1];
	float z = 60.0 * fwdDirection[2];
	
	output[0] = startpos[0] + x;
	output[1] = startpos[1] + y;
	output[2] = startpos[2] + z;
}

stock bool IsValidClient(int client)
{
	if(client <= 0)
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}
