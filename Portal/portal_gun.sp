#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>
#include <float>

#define BLUE 0
#define ORANGE 1

#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3
#define FLOOR 4
#define CEILING 5

#define MAX_BUTTONS 25

#define MODEL_BEAM "sprites/store/trails/water2.vmt"
#define MODEL_ERROR "models/error.mdl"

/*
	For the portals to be placed the map needs to have walls that are func_brush named 'portal_friendly' lined with a trigger_multiple named 'portal_trigger'. The walls need to be wider than they are deep as the portals will only work on the thin sides:
	
	  <portals>
	-------------
	|           |  <no portals>
	-------------
	  <portals>

	For a centre block that can be portalled on all sides you need to build four seperate wall setups.
*/

// These are the raw trigger properties used by the extended tractor beam
int g_rawSpineAxis[MAXPLAYERS+1];
int g_rawRadialAxis[MAXPLAYERS+1][2];
float g_rawRadialVector[MAXPLAYERS+1][3];
float g_rawStart[MAXPLAYERS+1][3];
float g_rawEnd[MAXPLAYERS+1][3];
float g_rawWidth[MAXPLAYERS+1];
bool g_rawTriggerActive[MAXPLAYERS+1];
bool g_rawTouching[MAXPLAYERS+1];
int g_rawTriggerTimer[MAXPLAYERS+1];
bool g_tractorExtOn;

StringMap g_gravityMap;
int g_tractorPortal[MAXPLAYERS+1];
int g_tractorExt[MAXPLAYERS+1][2]; // hmmm
int g_beamTargets[2]; // either side of the env_beam we are using for the portal'd tractor beam
int g_beamGravity; // entity ID for the the floaty part of a tractor beam
int g_beamFunnel; // entity ID for the visual aspect of a tractor beam
int g_beamBase;  // the point to fire tractor beam from on the map
int g_beamShell; // the casing for the beambase
int g_beamEnv; // the env_beam that must be toggled after info_targets moved

float g_velocity[MAXPLAYERS+1][3];
float g_velocityLog[MAXPLAYERS+1][10];
int g_button[MAXPLAYERS+1];

bool g_hasGun[MAXPLAYERS+1];
new g_portal[MAXPLAYERS+1][2]; // prop entity
int g_portalOrientation[MAXPLAYERS+1][2];
char PortalModelPath[][] = 
{
	"models/effects/fakeportalring_blue.mdl",
	"models/effects/fakeportalring_orange.mdl"
};

new g_lastButtons[MAXPLAYERS+1];

public Plugin myinfo = 
{
	name = "Portal Gun",
	author = "iBreatheTea",
	description = "A simple version of the portal gun",
	version = "0.1",
	url = ""
};

public OnPluginStart() 
{
	PrecacheModel(MODEL_BEAM);

	HookEvent("player_spawn", PG_PlayerSpawn);
   
    // continuous portal test (should be moved to OnPlayerRunCmd?)
	HookEntityOutput("trigger_multiple", "OnTrigger", TouchTrigger);
	
	// currently a lot going on in here:
	HookEntityOutput("trigger_multiple", "OnStartTouch", StartTouchTrigger);
	HookEntityOutput("trigger_multiple", "OnEndTouch", EndTouchTrigger);
	HookEntityOutput("trigger_multiple", "OnEndTouchAll", PushButtonRelease);
	
	g_gravityMap = CreateTrie();
}

// -------------------------------
// register portal network devices
// -------------------------------
public OnMapStart()
{
	PrecacheModel(MODEL_BEAM);
	
	int physmagic = -1;
	while ((physmagic = FindEntityByClassname(physmagic, "phys_thruster")) != -1)
	{
		if (!IsValidEntity(physmagic))
			continue;
		
		decl String:name[32];
		GetEntPropString(physmagic, Prop_Data, "m_iName", name, sizeof(name)); 
		
		// name of the cube points to it's grav thruster
		ReplaceString(name, 32, "thruster_", "", false);
		SetTrieValue(g_gravityMap, name, physmagic, false); 
	}
	
	int beambase = -1;
	while ((beambase = FindEntityByClassname(beambase, "func_brush")) != -1)
	{
		if (!IsValidEntity(beambase))
			continue;
		
		decl String:name[32];
		GetEntPropString(beambase, Prop_Data, "m_iName", name, sizeof(name)); 
		
		if (StrEqual(name, "tractor_beam_base", false))
		{
			g_beamBase = beambase;
			PrintToServer("Beam base secured");
		}
		
		if (StrEqual(name, "tractor_beam", false))
		{
			g_beamShell = beambase;
			PrintToServer("Beam base shell secured");
		}
	}
	
	int gravity = -1;
	while ((gravity = FindEntityByClassname(gravity, "trigger_multiple")) != -1)
	{
		if (!IsValidEntity(gravity))
			continue;
		
		decl String:name[32];
		GetEntPropString(gravity, Prop_Data, "m_iName", name, sizeof(name)); 
		
		if (StrEqual(name, "funnel_gravity_2", false))
		{
			g_beamGravity = gravity;
			PrintToServer("Beam gravity secured");
		}
	}

	g_beamTargets[0] = -1;
	g_beamTargets[1] = -1;
	g_tractorExtOn = false;
}


public Action PG_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	g_portal[client][BLUE] = -1;
	g_portal[client][ORANGE] = -1;
	g_hasGun[client] = false;
	g_tractorPortal[client] = -1;
	g_rawTriggerActive[client] = false;
	g_tractorExt[client][0] = -1;
	g_tractorExt[client][1] = -1;
	
	
	
	for (new i = 0; i < 10; ++i)
		g_velocityLog[client][i] = 0;
}

public void OnConfigsExecuted()
{
	//Precache
	PrecacheModel(PortalModelPath[0]);
	PrecacheModel(PortalModelPath[1]);
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if (!IsValidClient(client))
		return Plugin_Continue;
	
	// --- running tally for velocity ---
	for (new i = 9; i > 0; --i)
		g_velocityLog[client][i] = g_velocityLog[client][i-1];
	
	g_velocityLog[client][0] = GetVectorLength(g_velocity[client], false);
	
	// record current velocity
	GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", g_velocity[client]);
	// ----------------------------------
	
	RawTriggerTest(client);
	
	// Get every press/release of buttons
	for (new i = 0; i < MAX_BUTTONS; i++)
	{
		new button = (1 << i);
		if ((buttons & button))
		{
			if (!(g_lastButtons[client] & button))
			{
				ShootPortalPre(client, button);
			}
		}
	}
	
	g_lastButtons[client] = buttons;

	return Plugin_Continue;
}

ShootPortalPre(client, button)
{
	if (button != IN_ATTACK && button != IN_ATTACK2)
		return;
	
	new String:sWeaponName[64];
	GetClientWeapon(client, sWeaponName, sizeof(sWeaponName));
	if(!StrEqual(sWeaponName, "weapon_knifegg"))
		return;
	
	// can't trace the beam_wall because it is FSOLID_NOT_SOLID.
	// So we'll add and toggle a solid (nodraw) wall then change them back after
	new beamEnt = -1; 
	while ((beamEnt = FindEntityByClassname(beamEnt, "func_brush")) != -1)
	{
		if (!IsValidEntity(beamEnt))
			continue;

		decl String:name[32];
		GetEntPropString(beamEnt, Prop_Data, "m_iName", name, sizeof(name)); 
		
		if (StrEqual(name, "trace_wall", false))
		{
			SetVariantString("OnUser1 !self,Toggle,,0.0,1");
			AcceptEntityInput(beamEnt, "AddOutput");
			AcceptEntityInput(beamEnt, "FireUser1");
		}
	}
	
	g_button[client] = button;
	CreateTimer(0.0, ShootPortal, client);
}
	
public Action:ShootPortal(Handle:timer, any:client)
{
	int button = g_button[client];
	
	// Send trace
	float portalOrigin[3];
	int entity = BounceTraceBeam(client, portalOrigin);
	
	// Disable the trace walls again
	new beamEnt = -1; 
	while ((beamEnt = FindEntityByClassname(beamEnt, "func_brush")) != -1)
	{
		if (!IsValidEntity(beamEnt))
			continue;

		decl String:name[32];
		GetEntPropString(beamEnt, Prop_Data, "m_iName", name, sizeof(name)); 
		
		if (StrEqual(name, "trace_wall", false))
		{
			SetVariantString("OnUser1 !self,Toggle,,0.0,1");
			AcceptEntityInput(beamEnt, "AddOutput");
			AcceptEntityInput(beamEnt, "FireUser1");
		}
	}
	
	// leave if bad entity or non-portal surface
	decl String:name[1024];
	GetEntPropString(entity, Prop_Data, "m_iName", name, sizeof(name));
	if (entity == -1 || !StrEqual(name, "portal_friendly", true))
		return;
	
	int portalType = BLUE;
	if (button == IN_ATTACK2)
		portalType = ORANGE;
	
	float clientPos[3];
	GetEntPropVector(client, Prop_Data, "m_vecOrigin", clientPos);
	
	// surface structure information
	float startVec[3], endVec[3];
	GetEntPropVector(entity, Prop_Data, "m_vecMins", startVec);
	GetEntPropVector(entity, Prop_Data, "m_vecMaxs", endVec);
	float xwidth = FloatAbs(startVec[0]) + FloatAbs(endVec[0]);
	float ywidth = FloatAbs(startVec[1]) + FloatAbs(endVec[1]);
	float zwidth = FloatAbs(startVec[2]) + FloatAbs(endVec[2]);
	
	// portal position relative to the client
	float localPortalPos[3];
	localPortalPos[0] = portalOrigin[0] - clientPos[0];
	localPortalPos[1] = portalOrigin[1] - clientPos[1];
	localPortalPos[2] = portalOrigin[2] - clientPos[2];
	
	// get angle of surface
	float portalAngles[3];
	GetEntPropVector(entity, Prop_Data, "m_angRotation", portalAngles);
	
	if (xwidth < min(ywidth, zwidth))
	{
		if (localPortalPos[0] <= 0)
		{
			g_portalOrientation[client][portalType] = SOUTH;
		}
		else
		{
			portalAngles[1] += 180;
			g_portalOrientation[client][portalType] = NORTH;
		}
	}
	else if (ywidth < min(xwidth, zwidth))
	{
		if (localPortalPos[1] <= 0)
		{
			portalAngles[1] += 90;
			g_portalOrientation[client][portalType] = WEST;
		}
		else
		{
			portalAngles[1] += 270;
			g_portalOrientation[client][portalType] = EAST;
		}
	}
	else if (zwidth < min(xwidth, ywidth))
	{
		if (localPortalPos[2] <= 0)
		{
			portalAngles[0] -= 90;
			g_portalOrientation[client][portalType] = FLOOR;
		}
		else
		{
			portalAngles[0] += 90;
			g_portalOrientation[client][portalType] = CEILING;
		}
	}
	else
	{
		// account for perfect cube
		return;
	}
	
	// raise up from the floor
	portalAngles[0] += 90;

	GeneratePortal(client, portalOrigin, portalAngles, portalType);
}

GeneratePortal(client, Float:origin[3], Float:angles[3], portalType)
{
	// clear previous portal
	if (g_portal[client][portalType] != -1)
	{
		RemoveEdict(g_portal[client][portalType]);
	}
	
	// Create corresponding portal
	g_portal[client][portalType] = CreateEntityByName("prop_dynamic");
	if (g_portal[client][portalType] == -1)
	{
		PrintToChatAll("Failed");
		return;
	}

	// name and collision
	SetEntPropString(g_portal[client][portalType], Prop_Data, "m_iName", "custom_portal");
 
	// Poition it correctly
	SetEntityModel(g_portal[client][portalType], PortalModelPath[portalType]);
	DispatchSpawn(g_portal[client][portalType]);
	TeleportEntity(g_portal[client][portalType], origin, angles, NULL_VECTOR);
	
	// Here we check for any beams passing through the portal
	TractorBeamTrace(client, g_beamBase);
}

bool TouchOwnerPortal(owner, entity)
{
	if (owner == -1)
		return false; // entity has no ownership (can't be matched to a portal)
	
	if (g_portal[owner][BLUE] == -1 || g_portal[owner][ORANGE] == -1 )
		return false;
	
	float entityPos[3], blueOrigin[3], orangeOrigin[3];
	GetEntPropVector(entity, Prop_Data, "m_vecOrigin", entityPos);
	GetEntPropVector(g_portal[owner][BLUE], Prop_Data, "m_vecOrigin", blueOrigin);
	GetEntPropVector(g_portal[owner][ORANGE], Prop_Data, "m_vecOrigin", orangeOrigin);

	// if player is the entity then get centre point 
	if (IsValidClient(entity))
	{
		float eyePos[3];
		GetClientEyePosition(entity, eyePos);
		entityPos[0] = (entityPos[0] + eyePos[0]) / 2.0;
		entityPos[1] = (entityPos[1] + eyePos[1]) / 2.0;
		entityPos[2] = (entityPos[2] + eyePos[2]) / 2.0;
	}

	// this is the "are you on a portal" part
	float blueDistance = GetVectorDistance(entityPos, blueOrigin);
	float orangeDistance = GetVectorDistance(entityPos, orangeOrigin);
	if (blueDistance < 50.0 && blueDistance < orangeDistance)
	{
		OnTouchPortal(g_portal[owner][BLUE], owner, entity);
		return true;
	}
	else if (orangeDistance < 50.0 && orangeDistance < blueDistance)
	{
		OnTouchPortal(g_portal[owner][ORANGE], owner, entity);
		return true;
	}
	
	return false;
}

public TouchTrigger(const String:name[], caller, client, Float:delay)
{
	decl String:TriggerName[32];
    GetEntPropString(caller, Prop_Data, "m_iName", TriggerName, sizeof(TriggerName));
	
	// Deal with the Portal Network
    if(StrContains(TriggerName, "portal") != -1)
	{
		if (!IsValidEntity(client))
			return;
		
		// for portal checking it is either the client or an object 
		// owned by the client that is matched up against the portal.
		int owner = client;
		if (!IsValidClient(client)) // valid entity that's not the client
		{
			for (int i = 0; i < MAXPLAYERS+1; i++)
			{
				if (TouchOwnerPortal(i, client))
					break;
			}
		}
		else
		{
			TouchOwnerPortal(owner, client);
		}
	}
	
	// Deal with the tractor beams
	if (StrContains(TriggerName, "funnel_gravity") != -1)
	{
		if (client == -1)
			return;
		
		decl String:entName[32];
		GetEntPropString(client, Prop_Data, "m_iName", entName, sizeof(entName)); 
		
		if (StrContains(entName, "platonic") != -1)
		{
			int antigrav;
			GetTrieValue(g_gravityMap, entName, antigrav);
			
			SetVariantString("OnUser1 !self,Activate,,0.0,1");
			AcceptEntityInput(antigrav, "AddOutput");
			AcceptEntityInput(antigrav, "FireUser1");
		}
		else
		{
			// Make sure entity is floating
			SetEntityMoveType(client, MOVETYPE_FLY);
		}
		
		float entityVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", entityVelocity);
		
		float funnelVelocity[3];
		funnelVelocity[0] = entityVelocity[0] / 2.0;
		funnelVelocity[1] = entityVelocity[1] / 2.0;
		funnelVelocity[2] = entityVelocity[2] / 2.0;
		
		float magnitude = 75.0;
		if (StrEqual(TriggerName, "funnel_gravity_2"))
		{
			if (g_tractorPortal[client] != -1)
			{
				int facing = g_portalOrientation[client][g_tractorPortal[client]];
				if (facing == NORTH)
					funnelVelocity[0] -= magnitude;
				else if (facing == EAST)
					funnelVelocity[1] -= magnitude;
				else if (facing == SOUTH)
					funnelVelocity[0] += magnitude;
				else if (facing == WEST)
					funnelVelocity[1] += magnitude;
				else if (facing == FLOOR)
					funnelVelocity[2] += magnitude;
				else if (facing == CEILING)
					funnelVelocity[2] -= magnitude;
				else
					PrintToChatAll("???");
			}
		}
		else
		{
			funnelVelocity[2] = magnitude;
		}

		TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, funnelVelocity);
	}
}

public StartTouchTrigger(const String:name[], caller, client, Float:delay)
{
    decl String:TriggerName[32];
    GetEntPropString(caller, Prop_Data, "m_iName", TriggerName, sizeof(TriggerName)); 
	
	if (StrEqual(TriggerName, "weight_button") || StrEqual(TriggerName, "weighted_button"))
	{
		g_tractorExtOn = true;
		if (IsValidClient(client))
		{
			if (g_tractorExtOn)
				AcceptEntityInput(g_tractorExt[client][0],"TurnOn");
			else
				AcceptEntityInput(g_tractorExt[client][0],"TurnOff");
		}
	}

	if (StrContains(TriggerName, "funnel_gravity") != -1 ||
	    StrContains(TriggerName, "funnel_gravity_2") != -1 || 
	    StrEqual(TriggerName, "portal_trigger"))
	{
		TouchTrigger(name, caller, client, delay);
	}

	// walking through a remover beam
	if(StrEqual(TriggerName, "portal_remover"))
	{
		if (g_portal[client][BLUE] != -1)
		{
			RemoveEdict(g_portal[client][BLUE]);
			g_portal[client][BLUE] = -1;
		}
		
		if (g_portal[client][ORANGE] != -1)
		{
			RemoveEdict(g_portal[client][ORANGE]);
			g_portal[client][ORANGE] = -1;
		}
	}
	
	return Plugin_Continue;
}


public PushButtonRelease(const String:name[], caller, entity, Float:delay)
{
	decl String:TriggerName[32];
    GetEntPropString(caller, Prop_Data, "m_iName", TriggerName, sizeof(TriggerName)); 
	if (StrEqual(TriggerName, "weight_button") || StrEqual(TriggerName, "weighted_button"))
	{
		g_tractorExtOn = false;
		if (IsValidClient(entity))
		{
			if (g_tractorExtOn)
				AcceptEntityInput(g_tractorExt[entity][0],"TurnOn");
			else
				AcceptEntityInput(g_tractorExt[entity][0],"TurnOff");
		}
	}
}


public EndTouchTrigger(const String:name[], caller, entity, Float:delay)
{
	decl String:TriggerName[32];
    GetEntPropString(caller, Prop_Data, "m_iName", TriggerName, sizeof(TriggerName)); 
	
	decl String:entName[32];
    GetEntPropString(entity, Prop_Data, "m_iName", entName, sizeof(entName)); 

	if (StrContains(TriggerName, "funnel_gravity") != -1 || StrContains(TriggerName, "funnel_gravity_2") != -1)
	{
		// Make sure entity is NOT floating
		if (IsValidClient(entity))
			SetEntityMoveType(entity, MOVETYPE_ISOMETRIC);
		else
		{
			SetEntityMoveType(entity, MOVETYPE_VPHYSICS);
			
			if (StrContains(entName, "platonic") != -1)
			{
				int antigrav;
				GetTrieValue(g_gravityMap, entName, antigrav);
				
				SetVariantString("OnUser1 !self,Deactivate,,0.0,1");
				AcceptEntityInput(antigrav, "AddOutput");
				AcceptEntityInput(antigrav, "FireUser1");
			}
		}
	}
}

OnTouchPortal(portal, client, entity)
{
	int exitPortalType = BLUE;
	if (portal == g_portal[client][BLUE])
		exitPortalType = ORANGE;
	
	float exitAngle[3], entranceAngle[3], exitPortalAngles[3];
	GetEntPropVector(entity, Prop_Data, "m_angRotation", entranceAngle);
	GetEntPropVector(g_portal[client][exitPortalType], Prop_Data, "m_angRotation", exitPortalAngles);
	exitAngle[0] = entranceAngle[0];
	exitAngle[1] = exitPortalAngles[1];
	exitAngle[2] = entranceAngle[2];
	
	float exitOrigin[3], clientExitOrigin[3], magnitudeVelocity[3];
	GetEntPropVector(g_portal[client][exitPortalType], Prop_Data, "m_vecOrigin", exitOrigin);
	GetEntPropVector(g_portal[client][exitPortalType], Prop_Data, "m_vecOrigin", clientExitOrigin);
	GetEntPropVector(g_portal[client][exitPortalType], Prop_Data, "m_vecOrigin", magnitudeVelocity);
	
	// a generic speed value
	float entityVel[3];
	GetEntPropVector(entity, Prop_Data, "m_vecAbsVelocity", entityVel);
	float magnitude = GetVectorLength(entityVel);
	
	// if player is the entity then get centre point via an offset
	if (IsValidClient(entity))
	{
		float eyePos[3], clientPos[3], offset[3];
		GetEntPropVector(client, Prop_Data, "m_vecOrigin", clientPos);
		GetClientEyePosition(client, eyePos);
		offset[0] = clientPos[0] - ((clientPos[0] + eyePos[0]) / 2.0);
		offset[1] = clientPos[1] - ((clientPos[1] + eyePos[1]) / 2.0);
		offset[2] = clientPos[2] - ((clientPos[2] + eyePos[2]) / 2.0);
		
		clientExitOrigin[0] = clientExitOrigin[0] + offset[0];
		clientExitOrigin[1] = clientExitOrigin[1] + offset[1];
		clientExitOrigin[2] = clientExitOrigin[2] + offset[2];
		
		// just incase client is too fast and hits the wall
		magnitude = maxOfArray(g_velocityLog[client], 10);
	}
	
	// orientation control
	int facing = g_portalOrientation[client][exitPortalType];
	if (facing == NORTH)
	{
		clientExitOrigin[0] -= 50;
		magnitudeVelocity[0] -= magnitude;
	}
	else if (facing == EAST)
	{
		clientExitOrigin[1] -= 50;
		magnitudeVelocity[1] -= magnitude;
	}
	else if (facing == SOUTH)
	{
		clientExitOrigin[0] += 50;
		magnitudeVelocity[0] += magnitude;
	}
	else if (facing == WEST)
	{
		clientExitOrigin[1] += 50;
		magnitudeVelocity[1] += magnitude;
	}
	else if (facing == FLOOR)
	{
		clientExitOrigin[2] += 50;
		magnitudeVelocity[2] += magnitude;
	}
	else if (facing == CEILING)
	{
		clientExitOrigin[2] -= 200;
		magnitudeVelocity[2] -= magnitude;
	}
	else
		PrintToChatAll("???");
	
	float exitVelocity[3];
	MakeVectorFromPoints(exitOrigin, magnitudeVelocity, exitVelocity);
	TeleportEntity(entity, clientExitOrigin, exitAngle, exitVelocity);
}

public bool:TRDontHitSelf(entity, mask, any:data)
{
	if (entity == data)
		return false;
	
	return true;
}

stock void AddInFrontOf(float vecOrigin[3], float vecAngle[3], float units, float output[3])
{
	float vecAngVectors[3];
	vecAngVectors = vecAngle; //Don't change input
	GetAngleVectors(vecAngVectors, vecAngVectors, NULL_VECTOR, NULL_VECTOR);
	for (int i; i < 3; i++)
	output[i] = vecOrigin[i] + (vecAngVectors[i] * units);
}

stock bool IsValidClient(int client)
{
	if(client <= 0)
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}

// minimum of two values
stock float min(float foo, float bar)
{
	if (foo <= bar)
		return foo;
	
	return bar;
}

// maximum value in an array
stock maxOfArray(const arr[], len)
{
    new max = arr[0];
    for(new i = 1; i < len; ++i)
        if(arr[i] > max)
			max = arr[i];

		return max;
}

stock isSolid(entity)
{
	if ((GetEntProp(entity, Prop_Send, "m_nSolidType") != 0) && ((GetEntProp(entity, Prop_Send, "m_usSolidFlags") & 4) == 0))
		return true;
	
	return false;
}

// from AlliedModder 'backwards'
stock int BounceTraceBeam(client, float portalOrigin[3]) 
{ 
	new entity;
	new Handle:tr;
    float vecOrigin[3];
	float angRotation[3];
	GetClientEyePosition(client, vecOrigin);
	GetClientEyeAngles(client, angRotation);
	tr = TR_TraceRayFilterEx(vecOrigin, angRotation, MASK_SOLID, RayType_Infinite, TRDontHitSelf, client);

	// Get contact point
	entity = -1;
	float traceNormals[3];
	if (TR_DidHit(tr))
	{
		entity = TR_GetEntityIndex(tr);
		
		//Get Rotation of plane hit incase of reflection
		TR_GetPlaneNormal(tr, traceNormals);
		
		// get the actual point of interest
		TR_GetEndPosition(portalOrigin, tr);
	}
	CloseHandle(tr);
	
	decl String:entName[32];
    GetEntPropString(entity, Prop_Data, "m_iName", entName, sizeof(entName)); 
	if(StrEqual(entName, "portal_reflector"))
	{    
		float traceDirection[3], traceForward[3];
		
		//calculate angle of previous trace ray
		for(int i = 0;i < 3; i++)
			traceDirection[i] = portalOrigin[i] - vecOrigin[i];
		
		//convert to angle vector
		GetVectorAngles(traceDirection, traceDirection);
		
		//Get Forwards Direction.
		GetAngleVectors(traceDirection, traceForward, NULL_VECTOR, NULL_VECTOR);
		
		//... using suggested asherkin rocket bounce code
		new Float:dotProduct = GetVectorDotProduct(traceNormals, traceForward);
		
		ScaleVector(traceNormals, dotProduct);
		ScaleVector(traceNormals, 2.0);
		
		decl Float:vBounceVec[3];
		SubtractVectors(traceForward, traceNormals, vBounceVec);
		
		decl Float:reflectedDirection[3];
		GetVectorAngles(vBounceVec, reflectedDirection);

		tr = TR_TraceRayFilterEx(portalOrigin, reflectedDirection, MASK_SOLID, RayType_Infinite, TRDontHitSelf, entity);
		
		entity = -1;
		if (TR_DidHit(tr))
		{
			entity = TR_GetEntityIndex(tr);
			TR_GetEndPosition(portalOrigin, tr);
		}
		CloseHandle(tr);
	}

    return entity;
}

stock bool TractorBeamTrace(int client, int beamBase) 
{ 
	if (!IsValidEntity(g_beamBase))
		return false;
	
	new entity;
	new Handle:tr;
    float beamStart[3];
	float angRotation[3];
	GetEntPropVector(g_beamBase, Prop_Data, "m_vecOrigin", beamStart);
	GetEntPropVector(g_beamBase, Prop_Data, "m_angRotation", angRotation);
	angRotation[0] -= 90;
	tr = TR_TraceRayFilterEx(beamStart, angRotation, MASK_SOLID, RayType_Infinite, TRDontHitSelf, g_beamShell);

	// Get contact point
	entity = -1;
	float beamEnd[3];
	//float traceNormals[3];
	if (TR_DidHit(tr))
	{
		entity = TR_GetEntityIndex(tr);
		TR_GetEndPosition(beamEnd, tr);
	}
	CloseHandle(tr);
	

	int exitPortal = -1;
	decl String:entName[32];
    GetEntPropString(entity, Prop_Data, "m_iName", entName, sizeof(entName)); 
	if(StrEqual(entName, "portal_friendly") && g_tractorExtOn)
	{
		if (g_portal[client][BLUE] != -1 && g_portal[client][ORANGE] != -1)
		{
			float blueOrigin[3], orangeOrigin[3];
			GetEntPropVector(g_portal[client][BLUE], Prop_Data, "m_vecOrigin", blueOrigin);
			GetEntPropVector(g_portal[client][ORANGE], Prop_Data, "m_vecOrigin", orangeOrigin);
		
			float blueDistance = GetVectorDistance(beamEnd, blueOrigin);
			float orangeDistance = GetVectorDistance(beamEnd, orangeOrigin);
			if (blueDistance < 50.0 && blueDistance < orangeDistance)
			{
				exitPortal = g_portal[client][ORANGE];
				g_tractorPortal[client] = ORANGE;
			}
			else if (orangeDistance < 50.0 && orangeDistance < blueDistance)
			{
				exitPortal = g_portal[client][BLUE];
				g_tractorPortal[client] = BLUE;
			}
		}
	}
	
	// Clear any previous tracotr beam extensions
	g_rawTriggerActive[client] = false;
	if (g_tractorExt[client][0] != -1)
	{
		RemoveEdict(g_tractorExt[client][0]);
		g_tractorExt[client][0] = -1;
	}
		
	if (g_tractorExt[client][1] != -1)
	{
		RemoveEdict(g_tractorExt[client][1]);
		g_tractorExt[client][1] = -1;
	}
	
	// leave if the beam never passed through a portal
	if (exitPortal == -1)
	{
		g_tractorPortal[client] = -1;
		return true; // no portaling of beam
	}
	
	float exitPortalPos[3];
	float exitPortalAngles[3];
	GetEntPropVector(exitPortal, Prop_Data, "m_vecOrigin", exitPortalPos);
	GetEntPropVector(exitPortal, Prop_Data, "m_angRotation", exitPortalAngles);
	exitPortalAngles[0] -= 90;
	
	tr = TR_TraceRayFilterEx(exitPortalPos, exitPortalAngles, MASK_SOLID, RayType_Infinite, TRDontHitSelf, exitPortal);

	// Get contact point
	float secondBeamEnd[3];
	if (TR_DidHit(tr))
	{
		entity = TR_GetEntityIndex(tr);
		TR_GetEndPosition(secondBeamEnd, tr);
	}
	CloseHandle(tr);

	g_tractorExt[client][1] = CreateEntityByName("info_target"); 
    SetEntityModel( g_tractorExt[client][1], MODEL_BEAM );
    DispatchSpawn( g_tractorExt[client][1] );
    ActivateEntity(g_tractorExt[client][1]);
    TeleportEntity( g_tractorExt[client][1], secondBeamEnd, NULL_VECTOR, NULL_VECTOR );
    
    g_tractorExt[client][0] = CreateEntityByName( "env_beam" );
    SetEntityModel( g_tractorExt[client][0], MODEL_BEAM );
    DispatchKeyValue( g_tractorExt[client][0], "renderamt", "100" );
    DispatchKeyValue( g_tractorExt[client][0], "rendermode", "0" );
    DispatchKeyValue( g_tractorExt[client][0], "rendercolor", "94 184 251" );  
    DispatchKeyValue( g_tractorExt[client][0], "life", "0" ); 
    TeleportEntity( g_tractorExt[client][0], exitPortalPos, NULL_VECTOR, NULL_VECTOR ); 
    
    DispatchSpawn(g_tractorExt[client][0]);
    SetEntPropEnt( g_tractorExt[client][0], Prop_Send, "m_hAttachEntity", EntIndexToEntRef(g_tractorExt[client][0]), 0);
    SetEntPropEnt( g_tractorExt[client][0], Prop_Send, "m_hAttachEntity", EntIndexToEntRef(g_tractorExt[client][1]), 1);

    SetEntProp( g_tractorExt[client][0], Prop_Send, "m_nNumBeamEnts", 2);
    SetEntProp( g_tractorExt[client][0], Prop_Send, "m_nBeamType", 2);
    
    SetEntPropFloat( g_tractorExt[client][0], Prop_Data, "m_fWidth", 60.0 );
    SetEntPropFloat( g_tractorExt[client][0], Prop_Data, "m_fEndWidth", 60.0 );
	
	if (g_tractorExtOn)
		AcceptEntityInput(g_tractorExt[client][0],"TurnOn");
	
	// ------------------------------------
	// Set up gravity trigger 
	CreateRawTrigger(client, exitPortalPos, secondBeamEnd);

	return true;
}

// This function sets up the values for a cylindrical trigger area
// that is used by the portal'd tractor beam. This is per client.
void CreateRawTrigger(int client, float start[3], float end[3])
{
	if (g_tractorPortal[client] == -1)
		return;
	
	if (g_portalOrientation[client][g_tractorPortal[client]] == -1)
		return;
	
	g_rawRadialVector[client][0] = 0.0;
	g_rawRadialVector[client][1] = 0.0;
	g_rawRadialVector[client][2] = 0.0;
	
	float length = 0.0;
	int facing = g_portalOrientation[client][g_tractorPortal[client]];
	if (facing == NORTH)
	{
		g_rawSpineAxis[client] = 0;
		g_rawRadialAxis[client][0] = 1;
		g_rawRadialVector[client][1] = start[1];
		g_rawRadialAxis[client][1] = 2;
		g_rawRadialVector[client][2] = start[2];
	}
	else if (facing == EAST)
	{
		g_rawSpineAxis[client] = 1;
		g_rawRadialAxis[client][0] = 0;
		g_rawRadialVector[client][0] = start[0];
		g_rawRadialAxis[client][1] = 2;
		g_rawRadialVector[client][2] = start[2];
	}
	else if (facing == SOUTH)
	{
		g_rawSpineAxis[client] = 0;
		g_rawRadialAxis[client][0] = 1;
		g_rawRadialVector[client][1] = start[1];
		g_rawRadialAxis[client][1] = 2;
		g_rawRadialVector[client][2] = start[2];
	}
	else if (facing == WEST)
	{
		g_rawSpineAxis[client] = 1;
		g_rawRadialAxis[client][0] = 0;
		g_rawRadialVector[client][0] = start[0];
		g_rawRadialAxis[client][1] = 2;
		g_rawRadialVector[client][2] = start[2];
	}
	else if (facing == FLOOR)
	{
		g_rawSpineAxis[client] = 2;
		g_rawRadialAxis[client][0] = 0;
		g_rawRadialVector[client][0] = start[0];
		g_rawRadialAxis[client][1] = 1;
		g_rawRadialVector[client][1] = start[1];
	}
	else if (facing == CEILING)
	{
		g_rawSpineAxis[client] = 2;
		g_rawRadialAxis[client][0] = 0;
		g_rawRadialVector[client][0] = start[0];
		g_rawRadialAxis[client][1] = 1;
		g_rawRadialVector[client][1] = start[1];
	}
	else
		PrintToChatAll("???");
	
	g_rawStart[client][0] = start[0];
	g_rawStart[client][1] = start[1];
	g_rawStart[client][2] = start[2];
	
	g_rawEnd[client][0] = end[0];
	g_rawEnd[client][1] = end[1];
	g_rawEnd[client][2] = end[2];
	
	g_rawWidth[client] = 64.0;
	
	// finally begin checking every frame
	g_rawTriggerActive[client] = true;
}

void RawTriggerTest(int client)
{
	if (!g_rawTriggerActive[client])
		return;
	
	float clientPos[3];
	GetEntPropVector(client, Prop_Data, "m_vecOrigin", clientPos);
	
	// within either end of the trigger cylinder
	bool endTriggering = false;
	if ((clientPos[g_rawSpineAxis[client]] > g_rawStart[client][g_rawSpineAxis[client]] &&
	     clientPos[g_rawSpineAxis[client]] < g_rawEnd[client][g_rawSpineAxis[client]]) ||
		(clientPos[g_rawSpineAxis[client]] < g_rawStart[client][g_rawSpineAxis[client]] &&
	     clientPos[g_rawSpineAxis[client]] > g_rawEnd[client][g_rawSpineAxis[client]]))
	{
		clientPos[g_rawSpineAxis[client]] = 0;
		if (GetVectorDistance(clientPos, g_rawRadialVector[client]) < g_rawWidth[client])
		{
			// entering the trigger
			if (!g_rawTouching[client])
			{
				g_rawTouching[client] = true;
				StartTouchTrigger("raw trigger", g_beamGravity, client, 0.0);
				
				// Create an OnTrigger event while in this raw trigger
				g_rawTriggerTimer[client] = CreateTimer(0.02, Timer_OnTrigger, client, TIMER_REPEAT);
				
			}
		}
		else
		{
			// leaving the trigger
			if (g_rawTouching[client])
			{
				endTriggering = true;
			}
		}
	}
	else
	{
		// leaving the trigger
		if (g_rawTouching[client])
		{
			endTriggering = true;
		}
	}
	
	if (endTriggering)
	{
		// Kill the grab timer and reset control values
		if (g_rawTriggerTimer[client] != -1)
		{
			KillTimer(g_rawTriggerTimer[client]);
			g_rawTriggerTimer[client] = -1;
		}
		
		g_rawTouching[client] = false;
		EndTouchTrigger("raw trigger", g_beamGravity, client, 0.0);
	}
}

public Action Timer_OnTrigger(Handle timer, int client)
{
	// the continuous triggering while inside it
	TouchTrigger("raw trigger", g_beamGravity, client, 0.0);
} 