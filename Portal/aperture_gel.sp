#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>
#include <float>

bool g_bounce[MAXPLAYERS+1];
int g_fallSpeed[MAXPLAYERS+1];
int g_lastButtons[MAXPLAYERS+1];


public Plugin myinfo = 
{
	name = "Aperture Gels",
	author = "iBreatheTea",
	description = "A mod for simulating the bounce gel.",
	version = "1.0",
	url = ""
};

public OnPluginStart() 
{
	HookEvent("player_spawn", AG_PlayerSpawn);
	
	HookEntityOutput("trigger_multiple", "OnStartTouch", StartTouchTrigger);
	HookEntityOutput("trigger_multiple", "OnEndTouch", EndTouchTrigger);
}

public Action AG_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_bounce[client] = false;
	g_fallSpeed[client] = 0;
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if (!IsValidClient(client))
		return Plugin_Continue;
	
	bool changed = false;
	if (g_bounce[client])
	{
		// crouch landing stops the bounce
		if (!(buttons & IN_DUCK)) 
		{
			buttons |= IN_JUMP;
			changed = true;
		}
		
		g_bounce[client] = false;
	}
	
	g_lastButtons[client] = buttons;
	
	if (changed)
		return Plugin_Changed;
	
	return Plugin_Continue;
}

public StartTouchTrigger(const String:name[], trigger, entity, Float:delay)
{
    char triggerName[32];
    GetEntPropString(trigger, Prop_Data, "m_iName", triggerName, sizeof(triggerName)); 

	float entityVelocity[3];
	GetEntPropVector(entity, Prop_Data, "m_vecAbsVelocity", entityVelocity);
	
	if (IsValidClient(entity))
	{
		if (StrEqual(triggerName, "bluegel_bounce_parameter"))
		{
			g_fallSpeed[entity] = entityVelocity[2];
		}
		
		if (StrEqual(triggerName, "bluegel_bounce"))
		{
			if (g_fallSpeed[entity] != 0)
				g_bounce[entity] = true;
		}
	}
	else
	{
		if (StrEqual(triggerName, "bluegel_bounce"))
		{
			float bounce = FloatAbs(entityVelocity[2]);
			if (bounce < 600.0)
				bounce = 600.0;
			
			entityVelocity[2] = bounce;
			
			TeleportEntity(entity, NULL_VECTOR, NULL_VECTOR, entityVelocity);
		}
	}
	
	return Plugin_Continue;
}

public EndTouchTrigger(const String:name[], caller, entity, Float:delay)
{
	decl String:TriggerName[32];
    GetEntPropString(caller, Prop_Data, "m_iName", TriggerName, sizeof(TriggerName)); 
	
	decl String:entName[32];
    GetEntPropString(entity, Prop_Data, "m_iName", entName, sizeof(entName)); 

	if (StrEqual(TriggerName, "bluegel_bounce"))
	{
		// Make sure entity is NOT floating
		if (IsValidClient(entity))
		{
			float entityVelocity[3];
			GetEntPropVector(entity, Prop_Data, "m_vecAbsVelocity", entityVelocity);
			
			float bounce = FloatAbs(g_fallSpeed[entity]);
			
			if (bounce < 600.0)
				bounce = 600.0;
			
			entityVelocity[2] = bounce;
			
			TeleportEntity(entity, NULL_VECTOR, NULL_VECTOR, entityVelocity);
			
			g_fallSpeed[entity] = 0;
		}
	}
}

stock bool IsValidClient(int client)
{
	if(client <= 0)
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}
