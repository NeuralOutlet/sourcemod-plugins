#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>

new Float:g_ClientVel[MAXPLAYERS+1][3];
new bool:g_bClientShouldBoost[MAXPLAYERS+1];
new bool:g_bClientDoBoost[MAXPLAYERS+1];
new bool:g_bClientEnabled[MAXPLAYERS+1];

new Float:g_ArrVictimVel[MAXPLAYERS+1][128][3];
new g_ClientTicker[MAXPLAYERS+1];

new bool:g_bEnabled			= true;
new Float:g_fDudXSpeed		= 0.892;
new Float:g_fDudZSpeed		= 1.0;
new g_iDudDelayTicks 		= 1;

new Handle:g_hRemoveTimer[2048];

new bool:g_fountainsOn = false;
new bool:g_flipFlop = false;

new BeamSprite;

public Plugin:myinfo = 
{
	name		= "Flashboost Fountain",
	author		= "Mev (altered by iBreatheTea)",
	description	= "This plugin is mainly Mev's Flashboost code. Bespoke flashboost plugin used in kzx maps for the solo trikz flashboost fountains. Changes: does boost check when flashbang triggers OnTouched, shoots automatically our of specifically named brushes on the map, and adds a trail to the 'up' fountain.",
	version		= "1.0",
	url			= ""
}

public OnPluginStart()
{
	HookEvent("player_spawn", 	FF_PlayerSpawn);
}

public OnMapStart()
{
	g_fountainsOn = false;
	BeamSprite = PrecacheModel("materials/sprites/crystal_beam1.vmt");
}

public Action FF_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (!g_fountainsOn)
	{
		g_fountainsOn = true;
		
		int count = 0;
		new entity = -1; 
		while (entity = FindEntityByClassname(entity, "func_button"))
		{
			if (!IsValidEntity(entity))
				continue;

			decl String:name[32];
			GetEntPropString(entity, Prop_Data, "m_iName", name, sizeof(name)); 

			if (StrEqual(name, "flashboost_fountain_1", false))
			{
				CreateTimer(0.4, Timer_Flashboost, entity, TIMER_REPEAT);
				count = count + 1;
			}
			
			if (StrEqual(name, "flashboost_fountain_2", false))
			{
				CreateTimer(0.4, Timer_Flashboost, entity, TIMER_REPEAT);
				count = count + 1;
			}
			
			if (count == 2)
				break;
		}
		
		int vertflash = -1;
		while ((vertflash = FindEntityByClassname(vertflash, "func_brush")) != -1)
		{
			decl String:name[32];
			GetEntPropString(vertflash, Prop_Data, "m_iName", name, sizeof(name)); 
			if (StrEqual(name, "flashboost_fountain_up", false))
			{
				CreateTimer(0.5, Timer_Flashboost, vertflash, TIMER_REPEAT);
			}
		}
	}
}

/* Boost player */
public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if(IsValidClient(client))
	{
		decl Float:vClientVel[3];
		
		if(g_bEnabled)
		{
			if(g_ClientTicker[client] >= 128)
				g_ClientTicker[client] = 0;
			
			GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", vClientVel);
			g_ArrVictimVel[client][g_ClientTicker[client]] = vClientVel;
		}
		
		if(g_bClientShouldBoost[client] && g_bEnabled)
		{
			g_bClientDoBoost[client] = true;
			g_bClientShouldBoost[client] = false;
		}
		else if(g_bClientDoBoost[client] && g_bEnabled)
		{
			if(g_bClientEnabled[client])
			{
				TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, g_ClientVel[client]);
			}
			
			g_bClientDoBoost[client] = false;
		}
		g_ClientTicker[client]++;
	}
}

public Action:Timer_KillFlashbang(Handle:Timer, any:entity)
{
	if(IsValidEntity(entity) && entity != INVALID_ENT_REFERENCE)
		AcceptEntityInput(entity, "Kill");
	
	/* Close the other timer */
	if(g_hRemoveTimer[entity] != INVALID_HANDLE)
	{
		CloseHandle(g_hRemoveTimer[entity]);
		g_hRemoveTimer[entity] = INVALID_HANDLE;
	}
}


public Action Timer_Flashboost(Handle timer, entity)
{
	decl String:fountainName[32];
    GetEntPropString(entity, Prop_Data, "m_iName", fountainName, sizeof(fountainName)); 
	int zVelOffset = -500;
		
	// greate a grenade
	int flash = CreateEntityByName("flashbang_projectile");
	
	// Initial client deets
	SetVariantString("OnUser1 !self,InitializeSpawnFromWorld,,0.0,1");
	AcceptEntityInput(flash, "AddOutput");
	AcceptEntityInput(flash, "FireUser1");
	DispatchSpawn(flash);

	SetEntProp(flash, Prop_Data, "m_iTeamNum", CS_TEAM_T);
	
	float flashOrigin[3];
	float flashPos[3];
	GetEntPropVector(entity, Prop_Data, "m_vecOrigin", flashOrigin);
	
	// fountain cannon offset
	flashPos[0] = flashOrigin[0] + 5;
	flashPos[1] = flashOrigin[1];
	flashPos[2] = flashOrigin[2] + 10;
	
	float flashVelocity[3];
	GetEntPropVector(entity, Prop_Data, "m_vecVelocity", flashVelocity);
	
	if (StrEqual(fountainName, "flashboost_fountain_1", false))
	{
		flashVelocity[0] = flashVelocity[0] - zVelOffset;
		flashVelocity[1] = flashVelocity[1];
	}
	else if (StrEqual(fountainName, "flashboost_fountain_2", false))
	{
		flashVelocity[0] = flashVelocity[0];
		flashVelocity[1] = flashVelocity[1] + zVelOffset;
	}
	else
	{
		// verticle fountain trail
		TE_SetupBeamFollow(flash, BeamSprite, 0, 0.6, 6.0, 10.0, 4, {255,255,255,200});
		TE_SendToAll();
	}
	
	flashVelocity[2] = flashVelocity[2] + 700;
	
	float flashAngle[3];
	GetEntPropVector(entity, Prop_Data, "m_angRotation", flashAngle);

	TeleportEntity(flash, flashPos, flashAngle, flashVelocity); 
}

/* remove flashbangs before detonate */
public OnEntityCreated(entity, const String:classname[])
{
    if (StrContains(classname, "flashbang_projectile") != -1)
	{
		// flashboost
        SDKHook(entity, SDKHook_SpawnPost, OnSpawnPost);
		SDKHook(entity, SDKHook_Spawn, OnFlashSpawned);
	}
}

public OnFlashSpawned(flash)
{
    SDKHook(flash, SDKHook_Touch, OnFlashTouch);
}

public OnFlashTouch(flash, client)
{
    if(client > MaxClients)
		return;
	
    CheckFlashboost(flash, client);
} 

public OnSpawnPost(entity)
{
	g_hRemoveTimer[entity] = CreateTimer(1.45, Timer_KillFlashbang2, entity);
}

public Action:Timer_KillFlashbang2(Handle:Timer, any:entity)
{
	if(IsValidEntity(entity) && entity != INVALID_ENT_REFERENCE)
		AcceptEntityInput(entity, "Kill");
	
	g_hRemoveTimer[entity] = INVALID_HANDLE;
}

stock CheckFlashboost(flashbang, client)
{
	decl Float:flashOri[3], Float:victimOri[3], Float:victimVel[3];				
	
	GetEntPropVector(flashbang, Prop_Data, "m_vecAbsOrigin", flashOri);
	GetEntPropVector(client, Prop_Data, "m_vecAbsOrigin", victimOri);
	GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", victimVel);
	
	if(GetEngineVersion() == Engine_CSGO)
		victimOri[2] += 32.0;
	
	if(victimOri[2] >= flashOri[2] && victimVel[2] != 0)
	{
		decl Float:vBoost[3], Float:vVelFlash[3];
		decl iTick;
		
		iTick = g_ClientTicker[client] - g_iDudDelayTicks;
		if(iTick < 0)
			iTick += 128;
		
		GetEntPropVector(flashbang, Prop_Data, "m_vecAbsVelocity", vVelFlash);
		
		vBoost[0] = g_ArrVictimVel[client][iTick][0] - vVelFlash[0] * g_fDudXSpeed;
		vBoost[1] = g_ArrVictimVel[client][iTick][1] - vVelFlash[1] * g_fDudXSpeed;
		vBoost[2] = FloatAbs(vVelFlash[2] * g_fDudZSpeed);
		
		g_bClientShouldBoost[client] = true;
		
		g_ClientVel[client] = vBoost;
		
		TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, g_ClientVel[client]);
	}
	
	if(g_bClientEnabled[client])
		CreateTimer(0.0, Timer_KillFlashbang, flashbang);
}

/* Stocks */

stock bool IsValidClient(int client)
{
	if(client <= 0 )
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}
