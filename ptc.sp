#include <sourcemod>
#include <sdkhooks>
#include <cstrike>
#include <sdktools>

// the speed required to begin ABH
#define ABH_SPEED 230.0

bool g_hasGun[MAXPLAYERS+1];
bool g_ptcActive = true;
	
public Plugin myinfo = 
{
	name = "PTC Mod",
	author = "iBreatheTea",
	description = "Portal Test Chambers: logic puzzle rooms that require thinking with portals.",
	version = "1.0",
	url = "www.alliedmods.net"
}

public OnPluginStart() 
{
	HookEvent("player_spawn", 	PTC_PlayerSpawn);
	HookEntityOutput("func_button", "OnPressed", OnButtonPressed)
	HookEntityOutput("trigger_multiple", "OnStartTouch", OnStartTouchTrigger);
}

public OnMapStart()
{
	char currentMap[64];
	GetCurrentMap(currentMap, 64);
	
	if(StrContains(currentMap, "ptc_", false) == 0)
	{
		PrintToServer("Loading rules for ptc");
		g_ptcActive = true;
		
		InitialiseKZ(); // set default settings
		SetCvar("mp_startmoney", "0");
		SetCvar("mp_roundtime", "60");
		SetCvar("mp_t_default_melee", "0");
		SetCvar("mp_t_default_secondary", "0");
		SetCvar("bot_quota", "0");
		SetCvar("mp_limitteams", "0");
		SetCvar("mp_autoteambalance", "0");
		SetCvar("mp_respawn_on_death_t", "1");
		SetCvar("mp_respawn_on_death_ct", "1");
		SetCvar("sv_cheats", "1");
		SetCvar("mp_drop_knife_enable", "1");
		SetCvar("mp_warmuptime", "1");
		
		// Start with no weapons and no knife
		SetCvar("mp_t_default_primary", "");
		SetCvar("mp_t_default_secondary", "");
		SetCvar("mp_t_default_melee", "");

		SetCvar("mp_ct_default_primary", "");
		SetCvar("mp_ct_default_secondary", "");
		SetCvar("mp_ct_default_melee", "");
	}
	else
	{
		g_ptcActive = false;
	}
}

public Action PTC_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	g_hasGun[client] = false;
	
	// Negate damage
	SDKHook(client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
}

public Action OnButtonPressed(const String:output[], thing, client, Float:Any)
{
	if (!g_ptcActive)
		return Plugin_Continue;
	
	decl String:entity[1024];
	GetEntPropString(thing, Prop_Data, "m_iName", entity, sizeof(entity));
	
	// Link the PC/desk of weapons to buying
	if (StrEqual(entity,"collect_pknife", true))
	{
		if (g_hasGun[client])
			return Plugin_Continue;
		
		int Knif = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
		if(Knif != -1) 
		{
			CS_DropWeapon(client, Knif, false, true);
		}
		
		GivePlayerItem(client, "weapon_knifegg");
		g_hasGun[client] = true;
	}
	
	return Plugin_Continue;
}

public Action OnStartTouchTrigger(const String:name[], caller, client, Float:delay)
{
	if (!g_ptcActive)
		return Plugin_Continue;
	
	decl String:TriggerName[32];
    GetEntPropString(caller, Prop_Data, "m_iName", TriggerName, sizeof(TriggerName)); 
	
	// for aquiring the gold knife via the map
	if(StrEqual(TriggerName, "collect_pknife"))
	{
		if (!IsValidClient(client))
			return Plugin_Continue;
		
		if (g_hasGun[client])
			return Plugin_Continue;
		
		int Knif = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
		if(Knif != -1) 
		{
			CS_DropWeapon(client, Knif, false, true);
		}
		
		GivePlayerItem(client, "weapon_knifegg");
		g_hasGun[client] = true;
	}
	
	return Plugin_Continue;
}

void SetCvar(char[] scvar, char[] svalue)
{
	Handle cvar = FindConVar(scvar);
	SetConVarString(cvar, svalue, true);
}

void InitialiseKZ()
{
	// Cvars - KZ Settings
	SetCvar("sv_airaccelerate", "100");
	SetCvar("sv_maxvelocity", "5000"); // for crazy abh limits
	SetCvar("sv_accelerate", "6.5");       
	SetCvar("sv_friction", "5");  
	SetCvar("sv_maxspeed", "320");         
	SetCvar("sv_ladder_scale_speed", "1"); 
	SetCvar("sv_enablebunnyhopping", "1");
	SetCvar("sv_staminalandcost", "0");    
	SetCvar("sv_staminajumpcost", "0"); 
	SetCvar("sv_staminamax", "0");	
	
	// default other settings
	SetCvar("sv_accelerate_use_weapon_speed", "0");
}

// --- Accelerated Back Hopping (ABH) ---

public Action PreThink(int client)
{
	if(IsValidClient(client) && IsPlayerAlive(client) && g_ptcActive)
	{
		SetEntPropFloat(client, Prop_Send, "m_flStamina", 0.0); 
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if (!g_ptcActive)
		return Plugin_Continue;

	// check if player is alive and is in pressing space
	if (IsPlayerAlive(client) && buttons & IN_JUMP)
	{
		// about to jump while crouching
		if((GetEntityFlags(client) & FL_ONGROUND) && (GetEntityFlags(client)  & FL_DUCKING))
		{
			float velocity[3], angles[3];
			GetEntPropVector(client, Prop_Data, "m_vecVelocity", velocity);
			GetClientEyeAngles(client, angles);			
			float speed = GetVectorLength(velocity);

			if (speed >= ABH_SPEED)
			{
				float dot = GetVectorDotProduct(velocity, angles);
				if (dot < 0) // facing backwards
				{
					float scaleUp = 1.1;
					float scaleDown = 1.0 - (speed / ABH_SPEED);
					float excessVelocity[3];
					float boostVelocity[3];

					boostVelocity = velocity;
					excessVelocity = velocity;
					
					ScaleVector(excessVelocity, scaleUp);
					ScaleVector(excessVelocity, scaleDown);

					// velocity limit in current direction?
					float maxVelocity[3];
					maxVelocity[0] = velocity[0] - excessVelocity[0];
					maxVelocity[0] = velocity[1] - excessVelocity[1];

					// ABH calculations according to:
					// https://wiki.sourceruns.org/wiki/Accelerated_Back_Hopping
					// (your current speed) * 2 - flMaxSpeed
					ScaleVector(boostVelocity, 2.0);
					boostVelocity[0] -= maxVelocity[0];
					boostVelocity[1] -= maxVelocity[1];

					// retain vertical speed
					boostVelocity[2] = velocity[2];

					SetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", boostVelocity);
				}
			}
		}
	}

	return Plugin_Continue;
}

// Negate any damage
public Action Hook_OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3], int damagecustom)
{
	if (g_ptcActive)
		return Plugin_Handled;
	
	return Plugin_Continue;
}

stock bool IsValidClient(int client)
{
	if(client <= 0)
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}