## KZX Mod: Mapper's Guide ##

The maps have two routes: A KZ 'pure' route, and a KZX route. The latter will have large areas that a concjump/rocketjump or combos can be used to skip traditional KZing. I am persoanlly interested in KZ+conc runs, TFC concing always seemed to be the epitome of skillbased movement. KZX also includes Portals so check the PTC mappers guide for more on portal surfaces.

For making kzx\_ maps the following extras need to occour:

* A func\_button called 'console\_button' to make a buy station
* A trigger\_multiple called 'collect\_pknife' to reward players with the portal gun
* A func\_button called 'backpack\_refill' (with the optional suffix '\_a' or '\_b') to make ammo respawn points
* A trigger\_multiple called 'pure_kz_trigger' that blocks off the entrance to the yellow route

After that it's just up to the mapper's creativity to try and blend ll the movement styles into an interesting course.