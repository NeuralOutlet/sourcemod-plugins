## PTC Mod: Mapper's Guide ##

Portal-style puzzle maps are a little more involved, I'm trying my best to do in-mod solutions for as much as possible but it has been tough. Things like weighted buttons all need to be done in Hammer and I've not tried to implement a companion cube or cube-shaped-button.

For making ptc\_ maps (Portal Test Chambers) you need:

Portalable walls

* A func\_brush called 'portal\_friendly'
* A trigger\_multiple infront of it called 'portal\_trigger'

Currently walls need to be thinest on the side that you want to portal, so if you want a cube that is portalable on all six sides you will want to build six 1-unit thin walls around it. I'm currently using the brush's characteristics to determine its orientation.

Fizzler (partially working)

* A func\_brush that's never solid called 'beam\_wall' for the texture
* A trigger\_multiple called 'portal\_remover' for clearing player portals when they walk through
* A func\_brush textured nodraw and always solid that's called 'trace\_wall' that's disabled by default

The last of those three things is for blocking the portal beam, as a beam starts all the trace walls are enabled then the beam tries to fire. Afterwards they are disabled. This is done because a ray trace in the Source API won't ever touch a non-solid entity.

Weighted Cubes (Platonics)

* A prop\_physics that has the prefix 'platonic' using some cube model
* A phys\_thruster that has the prefix 'platonic\_thruster' connected to the platonic
    * Set to start off and to thrust 800 upwards to negate gravity
	
