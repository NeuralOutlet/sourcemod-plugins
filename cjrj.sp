#include <sourcemod>
#include <sdkhooks>
#include <cstrike>
#include <sdktools>

#define HEGrenadeOffset 14

char FULL_SOUND_PATH[] = "sound/custom_sounds/ammopickup2.wav";
char RELATIVE_SOUND_PATH[] = "custom_sounds/ammopickup2.wav";
bool g_cjrjActive;

public Plugin myinfo = 
{
	name = "TFC Jump Mod",
	author = "iBreatheTea",
	description = "TFC style conc jumping and CSGO style rocket jumping (with the nova). This is just the setup mod, this is paired with conc_jump_classic and rocket_jump mods.",
	version = "1.0",
	url = "www.alliedmods.net"
}

public OnPluginStart() 
{
	HookEntityOutput("func_button", "OnPressed", pressed);
	HookEvent("player_spawn", 	TFC_PlayerSpawn);
}

public OnMapStart()
{
	g_cjrjActive = false;
	
	char currentMap[64];
	GetCurrentMap(currentMap, 64);	
	if(StrContains(currentMap, "conc_", false) == 0 ||
	   StrContains(currentMap, "rj_", false)   == 0)
	{
		PrintToServer("Loading rules for conc/rj");
		g_cjrjActive = true;
		
		InitialiseKZ(); // set default settings
		SetCvar("mp_roundtime", "60");
		SetCvar("bot_quota", "0");
		SetCvar("mp_limitteams", "0");
		SetCvar("mp_autoteambalance", "0");
		SetCvar("mp_respawn_on_death_t", "1");
		SetCvar("mp_respawn_on_death_ct", "1");
		SetCvar("mp_drop_knife_enable", "1");
		SetCvar("mp_warmuptime", "1");
		SetCvar("sv_accelerate_use_weapon_speed", "1");
		
		SetCvar("ammo_grenade_limit_total", "3");
		SetCvar("sv_infinite_ammo", "1");
		
		PrecacheSound(RELATIVE_SOUND_PATH);
		AddFileToDownloadsTable(FULL_SOUND_PATH);
	}
}

public Action TFC_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	if (!g_cjrjActive)
		return Plugin_Handled;
	
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	int Prim = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
	
	if (GetClientTeam(client) == CS_TEAM_T)
	{
		// Rocket Jumper
		if(Prim != -1)
		{
			CS_DropWeapon(client, Prim, false, true);
		}
		
		GivePlayerItem(client, "weapon_nova");
	}
	else
	{
		// concer
		GivePlayerItem(client, "weapon_hegrenade");
		SetClientAmmo(client, HEGrenadeOffset, 3);
	}

	// Negate damage
	SDKHook(client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
	
	return Plugin_Handled;
}

// backpack refill

public pressed(const String:output[], thing, client, Float:Any)
{
	if (!g_cjrjActive)
		return Plugin_Handled;

	decl String:entity[1024];
	GetEntPropString(thing, Prop_Data, "m_iName", entity, sizeof(entity));
	
	if (StrEqual(entity,"backpack_refill", true))
	{
		RefillBackpack(client);
	}
	else if (StrEqual(entity,"backpack_refill_a", true))
	{
		RefillBackpack(client);
	}
	else if (StrEqual(entity,"backpack_refill_a", true))
	{
		RefillBackpack(client);
	}
	
	return Plugin_Handled;
}

RefillBackpack(client)
{
	// play the beep sound
	EmitSoundToClient(client, RELATIVE_SOUND_PATH, client);
	
	int weapon = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
	if (GetClientTeam(client) == CS_TEAM_T)
	{
		int weapon = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
		SetEntProp(weapon, Prop_Send, "m_iClip1", 4);
		SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 8);
	}
	
	if (GetClientTeam(client) == CS_TEAM_CT)
	{
		if (GetClientAmmo(client, HEGrenadeOffset) < 1)
			GivePlayerItem(client, "weapon_hegrenade");
		
		SetClientAmmo(client, HEGrenadeOffset, 3);
	}
}

// --- Abners bhop plugin ---

public Action PreThink(int client)
{
	if(IsValidClient(client) && IsPlayerAlive(client))
	{
		SetEntPropFloat(client, Prop_Send, "m_flStamina", 0.0); 
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if (IsPlayerAlive(client) && buttons & IN_JUMP) //Check if player is alive and is in pressing space
	{
		if(!(GetEntityMoveType(client) & MOVETYPE_LADDER) && !(GetEntityFlags(client) & FL_ONGROUND)) //Check if is not in ladder and is in air
		{
			if(waterCheck(client) < 2)
				buttons &= ~IN_JUMP; 
		}
	}
				
	return Plugin_Continue;
}

int waterCheck(int entity)
{
	return GetEntProp(entity, Prop_Data, "m_nWaterLevel");
}

public Action Hook_OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3], int damagecustom)
{
	// Negate any damage
	return Plugin_Handled;
}

stock bool IsValidClient(int client)
{
	if(client <= 0)
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}

// --------

void SetCvar(char[] scvar, char[] svalue)
{
	Handle cvar = FindConVar(scvar);
	SetConVarString(cvar, svalue, true);
}

void InitialiseKZ()
{
	// Cvars - KZ Settings
	SetCvar("sv_airaccelerate", "100");
	SetCvar("sv_maxvelocity", "2000");
	SetCvar("sv_accelerate", "6.5");       
	SetCvar("sv_friction", "5");  
	SetCvar("sv_maxspeed", "320");         
	SetCvar("sv_ladder_scale_speed", "1"); 
	SetCvar("sv_enablebunnyhopping", "1");
	SetCvar("sv_staminalandcost", "0");    
	SetCvar("sv_staminajumpcost", "0"); 
	SetCvar("sv_staminamax", "0");	
}

GetClientAmmo(client, offset)
{
	return GetEntProp(client, Prop_Data, "m_iAmmo", _, offset);
}

void SetClientAmmo(int client, int offset, int value)
{
	SetEntProp(client, Prop_Data, "m_iAmmo", value, _, offset);
}