#include <sourcemod>
#include <sdkhooks>
#include <cstrike>
#include <sdktools>

#define HEGrenadeOffset 14
#define TAGrenadeOffset 22

#define COST_PORTAL 10000
#define COST_ROCKET 1000
#define COST_CONC 600
#define COST_BHOP 400

public Plugin myinfo = 
{
	name = "KZX Mod",
	author = "iBreatheTea",
	description = "Buy menu, settings, and bhop for KZ eXtras.",
	version = "1.0",
	url = "www.alliedmods.net"
}

bool g_kzxActive;

// indicators for various eXtras
bool g_portaler[MAXPLAYERS+1];
bool g_rjumper[MAXPLAYERS+1];
bool g_bhopper[MAXPLAYERS+1];
bool g_concer[MAXPLAYERS+1];
bool g_KZer[MAXPLAYERS+1];
bool g_refills[MAXPLAYERS+1];

// timer has been pressed, for calculating expenses
// (timer menu handled in Other/timer_and_teleport.sp)
bool g_running[MAXPLAYERS+1];

char FULL_SOUND_PATH[] = "sound/custom_sounds/ammopickup2.wav";
char RELATIVE_SOUND_PATH[] = "custom_sounds/ammopickup2.wav";

public OnPluginStart() 
{
	HookEvent("player_spawn", 	KZX_PlayerSpawn);
	
	// yellow route stripper
	HookEntityOutput("trigger_multiple", "OnStartTouch", OnStartTouchTrigger);
	
	// Buy console
	HookEntityOutput("func_button", "OnPressed", pressed);
}

public OnMapStart()
{
	g_kzxActive = false;
	
	char currentMap[64];
	GetCurrentMap(currentMap, 64);	
	if(StrContains(currentMap, "kzx_", false) == 0)
	{
		PrintToServer("Loading rules for kzx");
		g_kzxActive = true;
		
		InitialiseKZ(); // set default settings
		SetCvar("mp_startmoney", "2000");
		SetCvar("mp_roundtime", "60");
		SetCvar("mp_t_default_melee", "0");
		SetCvar("mp_t_default_secondary", "0");
		SetCvar("bot_quota", "1");
		SetCvar("mp_limitteams", "0");
		SetCvar("mp_autoteambalance", "0");
		SetCvar("mp_respawn_on_death_t", "1");
		SetCvar("mp_respawn_on_death_ct", "1");
		SetCvar("sv_cheats", "1");
		SetCvar("mp_drop_knife_enable", "1");
		SetCvar("mp_warmuptime", "1");
		SetCvar("ammo_grenade_limit_total", "3");
		SetCvar("sv_accelerate_use_weapon_speed", "0");
		
		// Start with no weapons and no knife
		SetCvar("mp_t_default_primary", "");
		SetCvar("mp_t_default_secondary", "");
		SetCvar("mp_t_default_melee", "");

		SetCvar("mp_ct_default_primary", "");
		SetCvar("mp_ct_default_secondary", "");
		SetCvar("mp_ct_default_melee", "");
		
		PrecacheSound(RELATIVE_SOUND_PATH);
		AddFileToDownloadsTable(FULL_SOUND_PATH);
	}
}

public Action KZX_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_portaler[client] = false;
	g_bhopper[client] = false;
	g_rjumper[client] = false;
	g_concer[client] = false;
	g_KZer[client] = false;
	
	g_refills[client] = true;
	
	g_running[client] = false;

	// Negate damage
	SDKHook(client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
}

public Action OnStartTouchTrigger(const String:name[], caller, client, Float:delay)
{
	if (!g_kzxActive)
		return Plugin_Continue;
	
	decl String:TriggerName[32];
    GetEntPropString(caller, Prop_Data, "m_iName", TriggerName, sizeof(TriggerName)); 
	
	// for aquiring the gold knife via the map
	if(StrEqual(TriggerName, "pure_kz_trigger"))
	{
		if (!IsValidClient(client))
			return Plugin_Continue;
		
		SellAllExtras(client, false);
	}
}

public pressed(const String:output[], thing, client, Float:Any)
{
	if (!g_kzxActive)
		return Plugin_Handled;

	decl String:entity[1024];
	GetEntPropString(thing, Prop_Data, "m_iName", entity, sizeof(entity));
	
	// Link the PC/desk of weapons to buying
	if (StrEqual(entity,"console_button", true))
	{
		Panel_KZeXtras(client, 0);
	}
	
	if (StrEqual(entity,"timer_end", true))
	{
		g_running[client] = true;
	}
	
	if (StrEqual(entity,"timer_end", true))
	{
		if (g_running[client])
		{
			g_running[client] = false;
			int expenses = 0;
			if (g_portaler[client]) expenses += COST_PORTAL;
			if (g_rjumper[client]) expenses += COST_ROCKET;
			if (g_bhopper[client]) expenses += COST_BHOP;
			if (g_concer[client]) expenses += COST_CONC;
			
			char clientName[32];
			GetClientName(client, clientName, 32);
			PrintToChatAll("[%s] Cost: $%d", clientName, expenses);
		}
		
		if (!g_portaler[client])
		{
			g_portaler[client] = true;
			GivePlayerItem(client, "weapon_knifegg");
		}
	}
	
	// resupply ammo accordingly
	if (g_refills[client])
	{
		if (StrEqual(entity,"backpack_refill", true))
		{
			RefillBackpack(client);
		}
		else if (StrEqual(entity,"backpack_refill_a", true))
		{
			RefillBackpack(client);
		}
		else if (StrEqual(entity,"backpack_refill_a", true))
		{
			RefillBackpack(client);
		}
	}
	
	return Plugin_Handled;
}



public int PanelHandler1(Menu menu, MenuAction action, int client, int purchase)
{
    if (action == MenuAction_Select)
    {
		if (purchase != 8) // Exit
		{
			int primary = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
			int secondary = GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY);
			int knife = GetPlayerWeaponSlot(client, CS_SLOT_KNIFE);
			int grenade = GetPlayerWeaponSlot(client, CS_SLOT_GRENADE);	

			int discount = 1;
			if (!g_refills[client])
				discount = 2;

			switch(purchase)
			{
				case 1: // Portal Gun
				{
					DeductMoney(client, COST_PORTAL);
					StripWeapon(client, knife);
					
					GivePlayerItem(client, "weapon_knifegg");
					g_portaler[client] = true;
				}
				case 2: // Rocket Launcher
				{	
					DeductMoney(client, COST_ROCKET / discount);
					StripWeapon(client, primary);
					
					int weapon = GivePlayerItem(client, "weapon_nova");
					SetEntProp(weapon, Prop_Send, "m_iClip1", 4);
					SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 8);
					SDKHookEx(weapon, SDKHook_Reload, OnWeaponReload);
					g_rjumper[client] = true;
				}
				case 3: // CONC
				{
					DeductMoney(client, COST_CONC / discount);
					
					while(StripWeapon(client, grenade))
					{
						// strip all throwables
						grenade = GetPlayerWeaponSlot(client, CS_SLOT_GRENADE);	
					}
					
					GivePlayerItem(client, "weapon_hegrenade");
					SetClientAmmo(client, HEGrenadeOffset, 3);
					g_concer[client] = true;
				}
				case 4: //BHOP
				{
					DeductMoney(client, COST_BHOP);
					g_bhopper[client] = true;
				}
				case 5: // free usp for KZers
				{
					DeductMoney(client, 0);
					StripWeapon(client, secondary);
					
					GivePlayerItem(client, "weapon_usp_silencer");
					g_KZer[client] = true;
				}
				case 6: // toggle refill
				{
					// make sure no one buys cheap then toggles refill back on
					SellAllExtras(client, true); 
					
					g_refills[client] = !g_refills[client];
				}
				case 7: // Clear all
				{
					SellAllExtras(client, true);
				}
			}
			
			Panel_KZeXtras(client, 0);
		}
		else
		{
			// ...
		}
    }
    else if (action == MenuAction_Cancel)
    {
        PrintToServer("Client %d's menu was cancelled.  Reason: %d", client, purchase);
    }
}

// returns true if it successfully stripped a weapon
bool StripWeapon(client, weapon)
{
	if(weapon != -1)
	{
		CS_DropWeapon(client, weapon, false, true);
		RemoveEntity(weapon);
		return true;
	}
	
	return false;
}

// Called when a weapon is about to reload.
public Action:OnWeaponReload(weapon)
{
	if (!g_kzxActive)
		return Plugin_Continue;

	// If clip size is more than 0 and less than defined, deny weapon reloading
	int clipCount = GetEntProp(weapon, Prop_Send, "m_iClip1");
	if (clipCount >= 4) // clip has reached forced max of 4
	{
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
}

RefillBackpack(client)
{
	// play the beep sound
	EmitSoundToClient(client, RELATIVE_SOUND_PATH, client);
	
	int weapon = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
	if (g_rjumper[client] && weapon != -1)
	{
		int weapon = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
		SetEntProp(weapon, Prop_Send, "m_iClip1", 4);
		SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 8);
	}
	
	if (g_concer[client])
	{
		if (GetClientAmmo(client, HEGrenadeOffset) < 1)
			GivePlayerItem(client, "weapon_hegrenade");
		
		SetClientAmmo(client, HEGrenadeOffset, 3);
	}
}
 
public Action Panel_KZeXtras(int client, int args)
{
	new clientMoney = GetEntProp(client, Prop_Send, "m_iAccount");
	
	bool enableBhop = !g_bhopper[client];
	bool enableRJ = !g_rjumper[client];
	bool enablePort = !g_portaler[client];
	bool enableConc = !g_concer[client];
	bool enableKZ = !g_KZer[client];
	
    Panel panel = new Panel();
    panel.SetTitle("KZ eXtras Shop");
	
	int discount = 1; // no discount
	char refill[32] = "Refills             [On]";
	if (!g_refills[client])
	{
		refill = "Refills             [Off]";
		discount = 2; // 50% off!
	}
	
	DrawExtrasItem(panel, "Portal Gun (knife)", COST_PORTAL, clientMoney, enablePort);
	DrawExtrasItem(panel, "Rocket Launcher", COST_ROCKET / discount, clientMoney, enableRJ);
	DrawExtrasItem(panel, "Conc Grenades", COST_CONC / discount, clientMoney, enableConc);
	DrawExtrasItem(panel, "Bunny Tail", COST_BHOP, clientMoney, enableBhop);
	DrawExtrasItem(panel, "Free Pistol", 0, clientMoney, enableKZ);
	panel.DrawItem(refill);
	
	panel.DrawItem("Sell all extras");
	panel.DrawItem("Exit");
 
    panel.Send(client, PanelHandler1, 20);
 
    delete panel;
 
    return Plugin_Handled;
}

DeductMoney(client, amount) {
    
	new clientMoney = GetEntProp(client, Prop_Send, "m_iAccount"); 
	
	if (clientMoney >= amount) {
        clientMoney -= amount; 
		SetEntProp(client, Prop_Send, "m_iAccount", clientMoney); 
	}
}
#define MAX_MENU_ITEM 20
DrawExtrasItem(Panel panel, const String:item[], int cost, int clientMoney, bool:enable = true)
{
	decl String:menuItem[1024] = "";
	decl String:costStr[1024];
	IntToString(cost, costStr, 256);
	
	StrCat(menuItem, 1024, item);
	int spaces = MAX_MENU_ITEM - strlen(item);
	for (int i = 0; i < spaces; i++)
		StrCat(menuItem, 1024, " ");
	
	StrCat(menuItem, 1024, "$");
	StrCat(menuItem, 1024, costStr);
	
	if ((cost <= clientMoney) && enable)
	{
		panel.DrawItem(menuItem);
	} 
	else
	{
		panel.DrawItem(menuItem, ITEMDRAW_DISABLED);
	}
}

void SetCvar(char[] scvar, char[] svalue)
{
	Handle cvar = FindConVar(scvar);
	SetConVarString(cvar, svalue, true);
}

void InitialiseKZ()
{
	// Cvars - KZ Settings
	SetCvar("sv_airaccelerate", "100");
	SetCvar("sv_maxvelocity", "2000");
	SetCvar("sv_accelerate", "6.5");       
	SetCvar("sv_friction", "5");  
	SetCvar("sv_maxspeed", "320");         
	SetCvar("sv_ladder_scale_speed", "1"); 
	SetCvar("sv_enablebunnyhopping", "1");
	SetCvar("sv_staminalandcost", "0");    
	SetCvar("sv_staminajumpcost", "0"); 
	SetCvar("sv_staminamax", "0");	
	
	// default other settings
	SetCvar("sv_accelerate_use_weapon_speed", "1");
	SetCvar("sv_wateraccelerate", "10");
	SetCvar("sv_autobunnyhopping", "0");
	SetCvar("sv_water_swim_mode", "0");
	SetCvar("sv_waterfriction", "1");
}

void SellAllExtras(int client, bool sellPistol)
{
	int clientMoney = GetEntProp(client, Prop_Send, "m_iAccount"); 
	
	int discount = 1;
	if (!g_refills[client])
		discount = 2;
			
	if (g_bhopper[client])
	{
		g_bhopper[client] = false;
		clientMoney += COST_BHOP;
	}
	
	if (g_concer[client])
	{
		StripWeapon(client, GetPlayerWeaponSlot(client, CS_SLOT_GRENADE));
		SetClientAmmo(client, HEGrenadeOffset, 0);
		
		g_concer[client] = false;
		clientMoney += COST_CONC / discount;
	}
	
	if (g_rjumper[client])
	{
		StripWeapon(client, GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY));
		g_rjumper[client] = false;
		clientMoney += COST_ROCKET / discount;
	}
	
	if (g_portaler[client])
	{
		StripWeapon(client, GetPlayerWeaponSlot(client, CS_SLOT_KNIFE));
		g_portaler[client] = false;
		clientMoney += COST_PORTAL;
	}

	if (sellPistol && g_KZer[client])
	{
		StripWeapon(client, GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY));
		g_KZer[client] = false;
	}
	
	g_portaler[client] = false;

	SetEntProp(client, Prop_Send, "m_iAccount", clientMoney);
}

// --- Abners bhop plugin ---

public Action PreThink(int client)
{
	if (!g_kzxActive)
		return Plugin_Continue;
	
	if(IsValidClient(client) && IsPlayerAlive(client) && g_bhopper[client])
	{
		SetEntPropFloat(client, Prop_Send, "m_flStamina", 0.0); 
	}
	
	return Plugin_Handled;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if (!g_bhopper[client] || !g_kzxActive)
		return Plugin_Continue;
	
	if (IsPlayerAlive(client) && buttons & IN_JUMP) //Check if player is alive and is in pressing jump
	{
		if(!(GetEntityMoveType(client) & MOVETYPE_LADDER) && !(GetEntityFlags(client) & FL_ONGROUND)) //Check if is not in ladder and is in air
		{
			if(waterCheck(client) < 2)
				buttons &= ~IN_JUMP; 
		}
	}
				
	return Plugin_Continue;
}

int waterCheck(int entity)
{
	return GetEntProp(entity, Prop_Data, "m_nWaterLevel");
}

public Action Hook_OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3], int damagecustom)
{
	if (!g_kzxActive)
		return Plugin_Continue;
	
	// Negate any damage
	return Plugin_Handled;
}

stock bool IsValidClient(int client)
{
	if(client <= 0)
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}

GetClientAmmo(client, offset)
{
	return GetEntProp(client, Prop_Data, "m_iAmmo", _, offset);
}

SetClientAmmo(client, offset, value)
{
	return SetEntProp(client, Prop_Data, "m_iAmmo", value, _, offset);
}