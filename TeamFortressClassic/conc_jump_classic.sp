#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>

// Constants
#define CONC_MODEL_PATH "models/tfc_conc/classic_conc.mdl"
#define FULL_SOUND_PATH "sound/custom_sounds/timer.wav"
#define RELATIVE_SOUND_PATH "custom_sounds/timer.wav"
#define HEGrenadeOffset 14 // liable to change by Valve
#define MAX_DISTANCE 300.0

// Conc data
bool g_primedNade[MAXPLAYERS+1];
float g_concVelocity;
float g_boomScale;
bool g_ammoLock;

// --------------------
// --- Meta section ---
// --------------------

public Plugin myinfo = 
{
	name = "TFC Concs",
	author = "iBreatheTea",
	description = "Transforms CS:GO frag grenades into Team Fotress Classic style concussion grenades. Bind a key (I use 'f') to 'conc' in the console. Hold down your conc key to prime it, release to throw it.",
	version = "1.0",
	url = ""
};

public void OnPluginStart()
{
	g_concVelocity = CreateConVar("cj_throw_velocity", "1000", "velocity of conc throw");
	g_boomScale = CreateConVar("cj_boom_scale", "1000", "scale of conc explosion");
	g_ammoLock = CreateConVar("cj_ammo_lock", "1", "Ignore sv_infinite_ammo and bind max concs at ammo count");
	
	RegConsoleCmd("+conc", prime_conc);
	RegConsoleCmd("-conc", throw_conc);
}

public void OnMapStart()
{
	PrecacheSound(RELATIVE_SOUND_PATH);
	AddFileToDownloadsTable(FULL_SOUND_PATH);
	
	PrecacheModel(CONC_MODEL_PATH);
	AddFileToDownloadsTable(CONC_MODEL_PATH);
}

// -----------------
// --- Knockback ---
// -----------------

public void Knockback(int client, float distance, float clientPos[3], float explodePos[3])
{
	if(distance < MAX_DISTANCE)
	{
		//Create velocity
		float velocity[3];
		MakeVectorFromPoints(clientPos, explodePos, velocity);
		
		NormalizeVector(velocity, velocity);
		
		float clientVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", clientVelocity);
		
		ScaleVector(velocity, GetConVarFloat(g_boomScale));
		AddVectors(velocity, clientVelocity, velocity);
		
		TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, velocity);
	}
}


// ---------------------------
// --- TFC Style Throwing  ---
// ---------------------------
// Two seperate pipelines: priming and throwing
// 1) prime -> initialise -> detonate
// 2) throw 
// The player must begin priming before throwing, so
// the InstantDetonate function deals with situations
// when the conc is still being primed and then kills the throw
// ---------------------------


public Action prime_conc(int client, int args)
{
	// no ammo
	int concCount = GetClientAmmo(client, HEGrenadeOffset);
	if (concCount == 0)
	{
		g_primedNade[client] = -1;
		return;
	}
	
	// create a grenade
	int entity = CreateEntityByName("hegrenade_projectile");
	if(!IsValidClient(client))
	{
		g_primedNade[client] = -1;
		return;
	}
	
	// Spend the ammo
	if (GetConVarFloat(g_ammoLock))
	{
		SetClientAmmo(client, HEGrenadeOffset, concCount - 1);
	}
	
	//Change grenade model
	if(!IsModelPrecached(CONC_MODEL_PATH))
	{
		PrintToChatAll("model not precached");
		PrecacheModel(CONC_MODEL_PATH);
	}

	SetEntityModel(entity, CONC_MODEL_PATH);

	// Initial entity information
	SetVariantString("OnUser1 !self,InitializeSpawnFromWorld,,0.0,1");
	AcceptEntityInput(entity, "AddOutput");
	AcceptEntityInput(entity, "FireUser1");
	DispatchSpawn(entity); 

	SetEntPropEnt(entity, Prop_Data, "m_hThrower", client);
	SetEntPropEnt(entity, Prop_Data, "m_hOwnerEntity", client);
	SetEntProp(entity, Prop_Data, "m_iTeamNum", GetClientTeam(client));
	
	// store currently primed nade
	g_primedNade[client] = entity;
	
	// initialise the clients currently priming conc
	CreateTimer(0.0, InitialiseConc, entity);
	
	// play the beep sound
	EmitSoundToClient(client, RELATIVE_SOUND_PATH, client);
}

public Action throw_conc(int client, int args)
{
	// no conc to throw
	int entity = g_primedNade[client]; 
	if (entity == -1)
		return;
	
	// no longer primed
	g_primedNade[client] = -1;
	
	//Get position from where to throw the conc
	float concSpawnPos[3], eyePos[3], eyeAngles[3];
	GetClientEyePosition(client, eyePos);
	GetClientEyeAngles(client, eyeAngles);
	AddInFrontOf(eyePos, eyeAngles, 10.0, concSpawnPos);
	
	//Get player velocity
	float clientVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", clientVelocity);
	
	//Create velocity for conc
	float nadeVelocity[3], nadeAngle[3];
	GetAngleVectors(eyeAngles, nadeVelocity, NULL_VECTOR, NULL_VECTOR);
	GetClientEyeAngles(client, nadeAngle);
	NormalizeVector(nadeVelocity, nadeVelocity);
	ScaleVector(nadeVelocity, GetConVarFloat(g_concVelocity));
	
	//Add player velocity to conc velocity
	AddVectors(nadeVelocity, clientVelocity, nadeVelocity);

	//throw conc
	TeleportEntity(entity, concSpawnPos, nadeAngle, nadeVelocity);
	SetEntityModel(entity, CONC_MODEL_PATH);
}

// --------------------
// --- Conc methods ---
// --------------------

public Action InitialiseConc(Handle timer, int conc)
{
	// stop explosion
	SetEntProp(conc, Prop_Data, "m_nNextThinkTick", -1);
	SetEntProp(conc, Prop_Data, "m_takedamage", 0);
	
	// beep timer
	CreateTimer(4.0, Instant_Detonate, conc);
}

public Action Instant_Detonate(Handle timer, int conc)
{
	if (!GetEntityClassname(conc, "hegrenade_projectile", 22))
		return;
	
	int client = GetEntPropEnt(conc, Prop_Send, "m_hOwnerEntity");
	if (!IsValidClient(client))
		return;

	float clientPos[3], nadePos[3];
	GetClientEyePosition(client, clientPos);

	if (g_primedNade[client] == conc) // handconc
	{
		g_primedNade[client] = -1; // no longer prime it
		
		float clientVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", clientVelocity);
		
		// We spawn it at a scaled reduction of their position 
		// using their current velocity so that it has 
		// a boost effect if they are moving or has
		// no effect if they are standing still
		nadePos[0] = clientPos[0] - (clientVelocity[0] * 0.1);
		nadePos[1] = clientPos[1] - (clientVelocity[1] * 0.1);
		nadePos[2] = clientPos[2] - (clientVelocity[2] * 0.1);
		
		TeleportEntity(conc, nadePos, NULL_VECTOR, NULL_VECTOR);
	}
	else // previously thrown conc
	{
		GetEntPropVector(conc, Prop_Data, "m_vecOrigin", nadePos);
	}
	
	// get the distance between natural or teleported conc
	float distance = GetVectorDistance(nadePos, clientPos);
	
	// damage and explode the conc
	SetEntProp(conc, Prop_Data, "m_takedamage", 2);
	SetEntProp(conc, Prop_Data, "m_iHealth", 1);
	SDKHooks_TakeDamage(conc, 0, 0, 1.0);
	Knockback(client, distance, nadePos, clientPos);
}

// ---------------------
// -- misc functions ---
// ---------------------

stock bool IsValidClient(int client)
{
	if(client <= 0 )
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}

void AddInFrontOf(float vecOrigin[3], float vecAngle[3], float units, float output[3])
{
	float vecAngVectors[3];
	vecAngVectors = vecAngle;
	GetAngleVectors(vecAngVectors, vecAngVectors, NULL_VECTOR, NULL_VECTOR);
	for (int i; i < 3; i++)
		output[i] = vecOrigin[i] + (vecAngVectors[i] * units);
}

int GetClientAmmo(int client, int offset)
{
	return GetEntProp(client, Prop_Data, "m_iAmmo", _, offset);
}

void SetClientAmmo(int client, int offset, int value)
{
	SetEntProp(client, Prop_Data, "m_iAmmo", value, _, offset);
}
