#pragma semicolon 1

/*
	This plugin is a Frankenstein's monster of other plugins
	and is thus broken down into sections accordingly:
		
		- The knockback mechanic (taken from Boomix's Rocket Jump Beta plugin)
		- The prime / cook mechanic (altered from RedSword's Grenade Delay plugin)
		- The handheld explosion (altered from IAmACow's Exploding Grenades plugin)
		- The keep grenades equiped mechanic
*/

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>

#define PLUGIN_AUTHOR "Nooral"
#define PLUGIN_VERSION "0.01"

#define EXPLODE_SOUND 			"custom_sounds/timer.wav"
#define EXPLODE_VOLUME			0.1
#define BULLET_SPEED			1000.0
#define MAX_DISTANCE			160.0
#define JUMP_FORCE_UP			8.0
#define JUMP_FORCE_FORW			1.20
#define JUMP_FORCE_BACK			1.25
#define JUMP_FORCE_MAIN			270.0
#define RUN_FORCE_MAIN			0.8
#define MAX_BUTTONS 25

#define LoopAllPlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

char FULL_SOUND_PATH[] = "sound/custom_sounds/timer.wav";
char RELATIVE_SOUND_PATH[] = "custom_sounds/timer.wav";

new g_lastButtons[MAXPLAYERS+1];
bool g_cancelThrow[MAXPLAYERS+1];
bool g_isPriming[MAXPLAYERS+1];

// throwing the grenade via inspect weapon
bool g_manualThrow[MAXPLAYERS+1];
bool g_manualThrowPost[MAXPLAYERS+1];

// server values
float g_concVelocity;

// --------------------
// --- Meta section ---
// --------------------

public Plugin myinfo = 
{
	name = "Rocket & Conc",
	author = PLUGIN_AUTHOR,
	description = "Transforms CS:GO frag grenades into Team Fotress Classic style concussion grenades. Hold down attack to prime the conc and it will boom and knockback after a timed delay (if not thrown before boom then it will go off in your hand).",
	version = PLUGIN_VERSION,
	url = ""
};

public void OnPluginStart()
{
	HookEvent("player_spawn", 	RJ_PlayerSpawn);
	
	HookEventEx("hegrenade_detonate", grenade);
	
	OnConfigsExecuted();
}

public bool:OnClientConnect(client, String:rejectmsg[], maxlen)
{
	g_manualThrowPost[client] = false;
	g_manualThrow[client] = false;
	g_cancelThrow[client] = false;
    g_isPriming[client] = false;
	
	return true;
}

public void OnMapStart()
{
	PrintToConsoleAll("Starting map and caching...");
	
	PrecacheSound(RELATIVE_SOUND_PATH);
	AddFileToDownloadsTable(FULL_SOUND_PATH);
}


// --- Knockback ---

public Action RJ_PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));

	int weapon = GetPlayerWeaponSlot(client, CS_SLOT_GRENADE);
	if(weapon <= 0 || !IsValidEdict(weapon))
	{
		GivePlayerItem(client, "weapon_hegrenade");
		weapon = GetPlayerWeaponSlot(client, CS_SLOT_GRENADE);
	}

	SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", weapon);
	OnConfigsExecuted();
}

public void RJ_Jump(int shooter, float distance, float clientORG[3], float explodeORG[3])
{

	//Check, how far is player from bullet
	if(distance < MAX_DISTANCE)
	{
		bool down = false;
		
		//Create velocity
		float velocity[3];
		MakeVectorFromPoints(clientORG, explodeORG, velocity);
		
		if(velocity[2] < 0)
			down = true;
		
		NormalizeVector(velocity, velocity);
		
		float clientVelocity[3];
		GetEntPropVector(shooter, Prop_Data, "m_vecVelocity", clientVelocity);
		
		ScaleVector(velocity, JUMP_FORCE_MAIN);
		AddVectors(velocity, clientVelocity, velocity);
			
		clientVelocity[2] = 0.0;
		velocity[2] = 0.0;
		
		if (clientVelocity[0] < 0) {
		    if (explodeORG[0] > clientORG[0]) {
				ScaleVector(velocity, JUMP_FORCE_FORW);
				
		    } else {
				ScaleVector(velocity, JUMP_FORCE_BACK);
		    }
		} else {
		    if (explodeORG[0] < clientORG[0]) {
				ScaleVector(velocity, JUMP_FORCE_FORW);
				
		    } else {
				ScaleVector(velocity, JUMP_FORCE_BACK);
		    }
		}
		
		
		if (clientVelocity[1] < 0) {
			
		    if (explodeORG[1] > clientORG[1])
				ScaleVector(velocity, JUMP_FORCE_FORW);
		    else
				ScaleVector(velocity, JUMP_FORCE_BACK);
				
		} else {
			
		    if (explodeORG[1] < clientORG[1])
				ScaleVector(velocity, JUMP_FORCE_FORW);
		    else
				ScaleVector(velocity, JUMP_FORCE_BACK);
				
		}
		
		if((GetEntityFlags(shooter) & FL_ONGROUND))
			ScaleVector(velocity, RUN_FORCE_MAIN);


		if(distance > 37.0)
		{
			if(velocity[2] > 0.0)
				velocity[2] = 1000.0 + (JUMP_FORCE_UP * (MAX_DISTANCE - distance));
			else
				velocity[2] = velocity[2] + (JUMP_FORCE_UP * (MAX_DISTANCE - distance));	
		} else {
			
			velocity[2] = velocity[2] + (JUMP_FORCE_UP * (MAX_DISTANCE - distance)) / 1.37;	
		}
		
		if(down)
			velocity[2] *= -1;
		
		TeleportEntity(shooter, NULL_VECTOR, NULL_VECTOR, velocity);

	}

}

public grenade(Handle:event, const String:eventname[], bool:dontBroadcast)
{
    new m_hThrower;
    new Float:xyz[3], Float:pos[3];

    xyz[0] = GetEventFloat(event, "x");
    xyz[1] = GetEventFloat(event, "y");
    xyz[2] = GetEventFloat(event, "z");

    new ent = MaxClients+1;

    float ShooterORG[3];
    float distance;
	int shooter;

    new String:clsname[30];
    strcopy(clsname, sizeof(clsname), eventname);
    ReplaceString(clsname, sizeof(clsname), "_detonate", "_projectile");

    while( (ent = FindEntityByClassname(ent, clsname)) != -1 )
    {
        GetEntPropVector(ent, Prop_Data, "m_vecOrigin", pos);
        m_hThrower =  GetEntPropEnt(ent, Prop_Send, "m_hThrower");

        if(!FloatCompare(xyz[0], pos[0]) && !FloatCompare(xyz[1], pos[1]) && !FloatCompare(xyz[2], pos[2]))
        {
            if(0 < m_hThrower <= MaxClients)
            {
				shooter = GetEntPropEnt(ent, Prop_Send, "m_hOwnerEntity");
				if (shooter == -1)
				{
					PrintToChatAll("unknown grenade owner");
				}
				else
				{
					GetClientEyePosition(shooter, ShooterORG);
					distance = GetVectorDistance(xyz, ShooterORG);
					
					RJ_Jump(shooter, distance, pos, ShooterORG);
				}
            }
            else
            {
                PrintToChatAll("NULL - %s %f %f %f", clsname, pos[0], pos[1], pos[2]);
            }
            break;
        }
    }
} 

public Action Hook_BulletSetTransmit(int entity, int client) 
{ 
	if(IsValidEntity(entity))
	{
		int shooter = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");	
		if(shooter != client)
			return Plugin_Handled;
	}
	
	return Plugin_Continue;
}

public Action OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3], int damagecustom)
{
	// Negate any damage
	return Plugin_Handled;
}

// ------------------------
// --- Prime/Cook Nades ---
// ------------------------

public OnClientDisconnect_Post(client)
{
	g_lastButtons[client] = 0;
	g_isPriming[client] = false;
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if (!IsValidClient(client))
		return Plugin_Continue;

	bool buttonsChanged = false;
	// Check various grenade situations where we need
	// to change the input to the buttons bitmask
	if ((buttons & IN_ATTACK) && g_cancelThrow[client])
	{ // Primed too long, force it to drop
		PrintToChatAll("cancel throw");
		g_cancelThrow[client] = false;
		buttons &= ~IN_ATTACK;
		buttonsChanged = true;
	}

	// Get every press/release of buttons
	for (new i = 0; i < MAX_BUTTONS; i++)
	{
		new button = (1 << i);
		if ((buttons & button))
		{
			if (!(g_lastButtons[client] & button))
			{
				PrintToChatAll("buttons: %d, button: %d", buttons, button);
				OnButtonPress(client, button);
			}
		}
		else if ((g_lastButtons[client] & button))
		{
			OnButtonRelease(client, button);
		}
	}

	g_lastButtons[client] = buttons;
	
	if (buttonsChanged)
		return Plugin_Changed;

	return Plugin_Continue;
}


OnButtonPress(client, button)
{
	if (!IsValidClient(client))
		return;

	new weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
	if (weapon == -1)
		return;
	
	new weaponIndex = GetEntProp(weapon, Prop_Send, "m_iItemDefinitionIndex");
	if ((button == IN_ATTACK) && weaponIndex == 44)
	{
		PrintToChatAll("Priming nade...");
		g_isPriming[client] = true; // begin prime
		g_cancelThrow[client] = false; // fresh throw limit
		CreateTimer( 4.0, Timer_Detonate, client );
	} 
}

OnButtonRelease(client, button)
{
	if (!IsValidClient(client))
		return;

	new weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
	if (weapon == -1)
		return;
	
	new weaponIndex = GetEntProp(weapon, Prop_Send, "m_iItemDefinitionIndex");
	if((button == IN_ATTACK) && weaponIndex == 44)
	{
		g_isPriming[client] = false;
		
		//If the grenade is yet to explode (true throw)
		PrintToChatAll("Nade released naturally...");
	}
}

//Trigger logic on grenade creation (Panda D:)
public OnEntityCreated(ent, const String:classname[])
{
	if(!IsValidEntity(ent))
		return;
	
	if(StrEqual(classname, "weapon_hegrenade"))
	{
		SDKHook(ent, SDKHook_Spawn, OnEntitySpawned);
	}
}

public OnEntitySpawned(ent)
{
	SDKUnhook(ent, SDKHook_Spawn, OnEntitySpawned);
	new iRef = EntIndexToEntRef(ent);
	CreateTimer(0.0, Timer_OnGrenadeCreated, iRef);
	//CreateTimer(4.0, Instant_Detonate, iRef);
}

//Stop the grenade from thinking; by Panda
public Action:Timer_OnGrenadeCreated(Handle:timer, any:ref)
{
	new ent = EntRefToEntIndex( ref );
	if ( ent != INVALID_ENT_REFERENCE )
		SetEntProp(ent, Prop_Data, "m_nNextThinkTick", -1);
}

// SpawnGrenadeOnLocation is taken directly from IAmACow's Exploding Grenades plugin
public void SpawnGrenadeOnLocation(const int thrower, bool instaboom)
{
	float fLoc[3];
	GetClientEyePosition(thrower, fLoc);
	
	int entity = CreateEntityByName("hegrenade_projectile");
	if(!IsValidClient(thrower))
		return;
	
	PrintToChatAll("spawnonloco");
	SetVariantString("OnUser1 !self,InitializeSpawnFromWorld,,0.0,1");
	AcceptEntityInput(entity, "AddOutput");
	AcceptEntityInput(entity, "FireUser1");
	DispatchSpawn(entity); 
	SetEntPropEnt(entity, Prop_Data, "m_hThrower", thrower);
	SetEntPropEnt(entity, Prop_Data, "m_hOwnerEntity", thrower); // ???
	SetEntProp(entity, Prop_Data, "m_iTeamNum", GetClientTeam(thrower));
	TeleportEntity(entity, fLoc, NULL_VECTOR, NULL_VECTOR);
	
	// Blow it up instantly
	if (instaboom)
		CreateTimer(0.00, Instant_Detonate, entity);
}

public Action:Instant_Detonate(Handle:timer, any:grenade)
{
	if (!IsValidClient(grenade))
		return;
	
	if (!GetEntityClassname(grenade, "hegrenade_projectile", 22))
		return;
	
	int client = GetEntPropEnt(grenade, Prop_Send, "m_hOwnerEntity");
	if (!IsValidClient(client))
		return;
	
	float clientPos[3], nadePos[3];
	GetClientEyePosition(client, clientPos);
	GetEntPropVector(grenade, Prop_Data, "m_vecOrigin", nadePos);
	int distance = GetVectorDistance(nadePos, clientPos);
	
	SetEntProp(grenade, Prop_Data, "m_takedamage", 2);
	SetEntProp(grenade, Prop_Data, "m_iHealth", 1);
	
	SDKHooks_TakeDamage(grenade, 0, 0, 1.0);
	RJ_Jump(client, distance, nadePos, clientPos);
}

public Action:Timer_Detonate(Handle:timer, any:client)
{
	if (!IsValidClient(client))
	{
		PrintToChatAll("returning early...");
		return;
	}
	
	if (!g_isPriming[client])
	{
		PrintToChatAll("nade already thrown, no boom boom...");
		return;
	}
	
	PrintToChatAll("ticktickboom");
	// cancel currently held nade prime (throw mechanic IN_ATTACK)
	g_cancelThrow[client] = true;
	
	SpawnGrenadeOnLocation(client, true);
}

// ------------------------------
// --- Keep grenades equipped ---
// ------------------------------

public keepNadesOut(Handle:event, const String:eventname[], bool:dontBroadcast)
{
	// ???
}

// ---------------------
// -- misc functions ---
// ---------------------

void SetCvar(char[] scvar, char[] svalue)
{
	Handle cvar = FindConVar(scvar);
	SetConVarString(cvar, svalue, true);
}

stock bool IsValidClient(int client)
{
	if(client <= 0 )
			return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}

public void OnConfigsExecuted()
{
	//Cvars - Meta settings
	//SetCvar("bot_quota", "0");
	SetCvar("sv_cheats", "1");
	SetCvar("sv_infinite_ammo",	"1");
	SetCvar("ammo_grenade_limit_total", "5");
}

void AddInFrontOf(float vecOrigin[3], float vecAngle[3], float units, float output[3])
{
	float vecAngVectors[3];
	vecAngVectors = vecAngle;
	GetAngleVectors(vecAngVectors, vecAngVectors, NULL_VECTOR, NULL_VECTOR);
	for (int i; i < 3; i++)
	output[i] = vecOrigin[i] + (vecAngVectors[i] * units);
}

void FakePrecacheSound(const char[] szPath)
{
	AddToStringTable(FindStringTable( "soundprecache" ), szPath);
}

