This is a collection of plugins I've wrote for personal use. There are currently two game modes I am working on: KZ eXtra and Test Chamber. Each feature used has its own sp file and folder category, the code is stripped to its bare minimum (except the portal gun) so other modders can more easily reuse or cargocode it. Then in this folder there are the parent mod plugins (cjrj/ptc/kzx) that set everything up like custom menus, rules, and server settings. There are also mapping guides if people wanted to use these plugins and make their own maps for them. 

The idea for KZ eXtra was to bring together movement mechanics from other Half-Life games (mainly concing in TFC) and it turns out there is a fully fledged game in the works doing exactly that: [Momentum Mod](https://momentum-mod.org/), check them out their stuff looks amazing.

Below is a description of the following game modes::

* Concjump / Rocketjump
* Portal Test Chamber
* KZ eXtra

# Concjump / Rocketjump #

Gameplay video: https://www.youtube.com/watch?v=nCVj45zXSxQ

The map is rj\_ambient, rj\_easyrun, ..., (and kzx\_template's conc\_speed section)

This mod doesn't have much to it, just emulating the old TFC jump maps. Terrorists get a rocket launcher (nova), and counter-terrorists get concussion grenades. There was already an established rocket jumping community in House of Climb and a plugin made available by modder Boomix so I didn't change it much. The rj\_ maps have infinite ammo but when I get round to fully porting conc\_speed maps they will use refill backpacks like in the kzx\_template gameplay video lower down on this page.


# Test Chamber #

Gameplay video: https://www.youtube.com/watch?v=AsOSJ8SOpZE

[![](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/ptc_template_start_small.jpg)](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/ptc_template_start.jpg)
[![](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/ptc_template_mirrors_small.jpg)](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/ptc_template_mirrors.jpg)
[![](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/ptc_template_outside_small.jpg)](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/ptc_template_outside.jpg)

The map is ptc\_template.

Your gold knife was jammed into the portal gun surging portal energy into it, now when you slash with the knife it fires out a portal beam. Solve puzzling test chambers in the world of KZ. Have you ever tried strafing out of a portal? In Portal games you can't and it hurts your soul, surfing in Portal games will also leave you feeling empty - but not anymore!

Current Portal Features:

* Portals 
* Fizzlers: [bug] currently blocks portal beam even if disabled
* Mirrors
* Blue gel (static only)
* Companion Cubes
* Tractor Beam
* ABH

Summary of differences:

The simple portal network has conservation of momentum, but shoots you out orthogonal to the wall. **Portals** have to be placed on N/S/E/W facing walls or floor/ceiling (no non-90 angles), this means there isn't any 45 degree portal boost jumps for now. The client's portals only effect the client, although all cubes pass through all portals. When you shoot a portal beam if it hits a **mirror** it will bounce off and try to place a portal on the next surface it hits. Note: the portal firing mechanic only does one reflection check. The **blue gel** will bounce the player a minimum of 600 velocity upwards, jumping into the gel will automatically bounce the player but if they crouch it won't effect them. landing into the gel with over 600 units downward velocity will bounce you with a matched velocity upwards. The **tractor beam** can travel through a portal and moves the player or cube in one direction (no reversed polarity yet). Accelerated Back Hop (**ABH**) exists although it is janky, it actively requires your speed reach 240 and that you are moving backwards while crouched hitting a bhop.


# KZ eXtra #

Gameplay video: https://www.youtube.com/watch?v=G2OyuTD8QEc

[![](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/kzx_template_red_small.jpg)](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/kzx_template_red.jpg)
[![](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/kzx_template_concspeed_small.jpg)](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/kzx_template_concspeed.jpg)
[![](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/kzx_template_yellow_small.jpg)](https://bitbucket.org/NeuralOutlet/sourcemod-plugins/raw/00ec67d8af59598b1f14c9ee2af6f15d742266da/map_previews/kzx_template_yellow.jpg)

The map is kzx\_template.

This mod extends the skillbased movement game mode, KZ, by including other noteable movement mechanics from other Half-Life universes. The traditional bhop/surf/longjump/ladderhop are joined by concjump/rocketjump/portals and a twist on flashboosting. At the start of the map you'll find a buy station where you can purchase the three new techniques and auto bhops, you have exactly the right money to buy everything except the portal gun (currently an end-map prize). The maps have two routes: A KZ 'pure' route, and a KZX route. The the KZX route will have large areas that concs / rockets can be used to skip traditional KZing. You chose if you want to run the map cheap or easy. You only get certain amount of concs/rockets so will have to pick up ammo bags along the way. Then at the end of the map you are greeted with the portal gun.

Flashboosting in KZX is found in the form of flashboost fountains that spew out flashbangs, catching one of these under your feet will boost you in the same way as it would in a trikz server.

