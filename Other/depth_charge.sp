#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>

#define PLUGIN_AUTHOR "iBreatheTea"
#define PLUGIN_VERSION "1.00"

#define MAX_DISTANCE			500.0
#define JUMP_FORCE_UP			8.0
#define JUMP_FORCE_FORW			1.20
#define JUMP_FORCE_BACK			1.25
#define JUMP_FORCE_MAIN			200.0
#define RUN_FORCE_MAIN			0.8

#define LoopAllPlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))
	
float g_forceMultiplier = JUMP_FORCE_MAIN;

public Plugin myinfo = 
{
	name = "Depth Charge",
	author = PLUGIN_AUTHOR,
	description = "Converts the Tactical Grenade into an underwater conc nade. If it detonates underwater then it creates a large knockback affect that can push the player up out the water. Knockback code taken from Boomix's Rocket Jump Beta plugin.",
	version = PLUGIN_VERSION,
	url = ""
};

public void OnPluginStart()
{
	g_forceMultiplier = CreateConVar( "dc_conc_force", "500.0", "How powerful is the depth charge when under water?", 
		_, true, 0.0 );
	
	HookEventEx("tagrenade_detonate", grenade);
}

// --- Knockback ---

public void Knockback(int shooter, float distance, float clientORG[3], float explodeORG[3])
{

	//Check, how far is player from bullet
	if(distance < MAX_DISTANCE)
	{
		bool down = false;
		
		//Create velocity
		float velocity[3];
		MakeVectorFromPoints(clientORG, explodeORG, velocity);
		
		if(velocity[2] < 0)
			down = true;
		
		NormalizeVector(velocity, velocity);
		
		float clientVelocity[3];
		GetEntPropVector(shooter, Prop_Data, "m_vecVelocity", clientVelocity);
		
		ScaleVector(velocity, GetConVarFloat(g_forceMultiplier));
		AddVectors(velocity, clientVelocity, velocity);
			
		clientVelocity[2] = 0.0;
		velocity[2] = 0.0;
		
		if (clientVelocity[0] < 0) {
		    if (explodeORG[0] > clientORG[0]) {
				ScaleVector(velocity, JUMP_FORCE_FORW);
				
		    } else {
				ScaleVector(velocity, JUMP_FORCE_BACK);
		    }
		} else {
		    if (explodeORG[0] < clientORG[0]) {
				ScaleVector(velocity, JUMP_FORCE_FORW);
				
		    } else {
				ScaleVector(velocity, JUMP_FORCE_BACK);
		    }
		}
		
		if (clientVelocity[1] < 0) {
			
		    if (explodeORG[1] > clientORG[1])
				ScaleVector(velocity, JUMP_FORCE_FORW);
		    else
				ScaleVector(velocity, JUMP_FORCE_BACK);
				
		} else {
			
		    if (explodeORG[1] < clientORG[1])
				ScaleVector(velocity, JUMP_FORCE_FORW);
		    else
				ScaleVector(velocity, JUMP_FORCE_BACK);
				
		}
		
		if((GetEntityFlags(shooter) & FL_ONGROUND))
			ScaleVector(velocity, RUN_FORCE_MAIN);


		if(distance > 37.0)
		{
			if(velocity[2] > 0.0)
				velocity[2] = 1000.0 + (JUMP_FORCE_UP * (MAX_DISTANCE - distance));
			else
				velocity[2] = velocity[2] + (JUMP_FORCE_UP * (MAX_DISTANCE - distance));	
		} else {
			
			velocity[2] = velocity[2] + (JUMP_FORCE_UP * (MAX_DISTANCE - distance)) / 1.37;	
		}
		
		if(down)
			velocity[2] *= -1;
		
		TeleportEntity(shooter, NULL_VECTOR, NULL_VECTOR, velocity);
	}

}

public grenade(Handle:event, const String:eventname[], bool:dontBroadcast)
{
    new m_hThrower;
    new Float:xyz[3], Float:pos[3];

    xyz[0] = GetEventFloat(event, "x");
    xyz[1] = GetEventFloat(event, "y");
    xyz[2] = GetEventFloat(event, "z");

    new ent = MaxClients+1;
    new String:clsname[30];
    strcopy(clsname, sizeof(clsname), eventname);
    ReplaceString(clsname, sizeof(clsname), "_detonate", "_projectile");

    while( (ent = FindEntityByClassname(ent, clsname)) != -1 )
    {
        GetEntPropVector(ent, Prop_Data, "m_vecOrigin", pos);
        m_hThrower =  GetEntPropEnt(ent, Prop_Send, "m_hThrower");

        if(!FloatCompare(xyz[0], pos[0]) && 
		   !FloatCompare(xyz[1], pos[1]) && 
		   !FloatCompare(xyz[2], pos[2]))
        {
            if(0 < m_hThrower <= MaxClients)
            {
				int client = GetEntPropEnt(ent, Prop_Send, "m_hOwnerEntity");
				if (client == -1)
				{
					PrintToChatAll("unknown grenade owner");
				}
				else
				{
					if(GetEntityFlags(client) & FL_INWATER)
					{
						float throwerPos[3];
						GetClientEyePosition(client, throwerPos);
						
						float distance = GetVectorDistance(xyz, throwerPos);
						Knockback(client, distance, pos, throwerPos);
					}
				}
            }

            break;
        }
    }
} 

