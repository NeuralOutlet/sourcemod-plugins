#include <sourcemod>
#include <sdkhooks>
#include <cstrike>
#include <sdktools>

float g_clientPositions[MAXPLAYERS+1][10][3];
float g_clientAngles[MAXPLAYERS+1][10][3];
float g_clientDuck[MAXPLAYERS+1][10];

float g_startTime[MAXPLAYERS+1];
float g_mapStart[3];

int g_hTimer[MAXPLAYERS+1];
bool g_timing[MAXPLAYERS+1];

public Plugin myinfo = 
{
	name = "Time and TP",
	author = "iBreatheTea",
	description = "Very simple timer and teleport utility for KZ runs.",
	version = "1.0",
	url = "www.alliedmods.net"
}

public OnPluginStart() 
{
	RegConsoleCmd("menu", Command_Timer);
	HookEvent("player_spawn", PlayerSpawn);
	HookEntityOutput("func_button", "OnPressed", pressed)
}

public OnMapStart()
{
	int beginning = -1;
	while ((beginning = FindEntityByClassname(beginning, "info_teleport_destination")) != -1)
	{
		if (!IsValidEntity(beginning))
			continue;
		
		decl String:name[32];
		GetEntPropString(beginning, Prop_Data, "m_iName", name, sizeof(name)); 
		
		if (StrEqual(name, "tp1", false)) 
		{
			GetEntPropVector(beginning, Prop_Data, "m_vecOrigin", g_mapStart);
		}
	}
}

public Action PlayerSpawn(Handle event, const char[] name, bool dontBroadcast) 
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	g_hTimer[client] = -1;
	g_timing[client] = false;
}


public pressed(const String:output[], thing, client, Float:Any)
{
	decl String:entity[1024];
	GetEntPropString(thing, Prop_Data, "m_iName", entity, sizeof(entity));
	
	if (StrEqual(entity,"timer_start", true))
	{
		g_timing[client] = true;
		Command_Timer(client, 0);
		TimerMenu(client);
	}
	
	if (StrEqual(entity,"timer_end", true))
	{
		if (g_hTimer[client] != -1)
		{
			KillTimer(g_hTimer[client]);
			g_hTimer[client] = -1;
			g_timing[client] = false;
			
			char clientName[32];
			GetClientName(client, clientName, 32);
			
			char szTime[32];
			GetClientRunTime(client, szTime, 32);
			PrintToChatAll("[%s] Time: %s", clientName, szTime);
		}
		else
		{
			PrintToChatAll("Finished without pressing the start timer :(");
		}
	}
	
	return Plugin_Handled;
}

public Action Command_Timer(int client, int args)
{
    if(!IsClientInGame(client))
        return Plugin_Handled;
	
	if (g_timing[client])
		g_startTime[client] = GetGameTime();
	
	if (g_hTimer[client] != -1)
		KillTimer(g_hTimer[client]);
    
    g_hTimer[client] = CreateTimer(0.01, Timer_Exemple, client, TIMER_REPEAT);

    return Plugin_Handled;
}

public Action Timer_Exemple(Handle timer, any client)
{
    if(!IsClientInGame(client))
        return Plugin_Stop;
    
    TimerMenu(client);
    
    return Plugin_Continue;
}

public int TimerMenuHandler(Menu menu, MenuAction action, int client, int purchase)
{
    if (action == MenuAction_Select)
    {
		switch(purchase)
		{
			case 0: // save tp
			{
				if(GetEntityFlags(client) & FL_ONGROUND)
				{
					float duckAmount = GetEntPropFloat(client, Prop_Send, "m_flDuckAmount");
					
					float position[3], angles[3];
					GetEntPropVector(client, Prop_Data, "m_vecOrigin", position);
					GetClientEyeAngles(client, angles);
					
					for (int i = 1; i < 9; i++)
					{
						g_clientPositions[client][i] = g_clientPositions[client][i-1];
						g_clientAngles[client][i] = g_clientAngles[client][i-1];
						g_clientDuck[client][i] = g_clientDuck[client][i-1];
					}

					g_clientPositions[client][0] = position;
					g_clientAngles[client][0] = angles;
					g_clientDuck[client][0] = duckAmount;
				}
			}
			case 1: // tp
			{
				float noVelocity[3] = { 0,0,0 };
				SetEntPropFloat(client, Prop_Send, "m_flDuckAmount", g_clientDuck[client][0]);
				TeleportEntity(client, g_clientPositions[client][0], g_clientAngles[client][0], noVelocity);
			}
			case 3: // tp to start
			{
				float noVelocity[3] = { 0,0,0 };
				TeleportEntity(client, g_mapStart, NULL_VECTOR, noVelocity);
			}
			
			case 5:
			{
				if (IsValidClient(client) && g_hTimer[client] != -1)
				{
					KillTimer(g_hTimer[client]);
					g_hTimer[client] = -1;
				}
			}
		}
    }
	
	if(action == MenuAction_End)
	{
        delete menu;
	}
}

public void TimerMenu(int client)
{
    Menu menu = new Menu(TimerMenuHandler);
	
	if (g_timing[client])
	{
		char szTime[32];
		GetClientRunTime(client, szTime, 32);
		menu.SetTitle(szTime);
	}
	else
	{
		menu.SetTitle("KZ eXtra");
	}
	
	menu.AddItem("chkpnt", "Checkpoint");     // 1
	menu.AddItem("tp", "Teleport");           // 2
	menu.AddItem("", " ", ITEMDRAW_DISABLED); // 3
	menu.AddItem("strt", "Start");            // 4
	menu.AddItem("", " ", ITEMDRAW_DISABLED); // 5
	menu.AddItem("exit", "Exit");             // 6

    menu.ExitButton = false;
    menu.Display(client, 1);
}

void GetClientRunTime(client, char runtime[32], int maxlength)
{
	float runTime = GetGameTime() - g_startTime[client];
	int mins = RoundToZero(runTime) / 60;
	int secs = RoundToZero(runTime) % 60;
	int msec = RoundToZero((runTime * 1000.0)) % 1000;
	
	FormatEx(runtime, sizeof(runtime), "%dm %d.%ds", mins, secs, msec);
}

stock bool:IsClientDuckStucked(client)
{
  decl Float:vMax[3];
  GetEntPropVector(client, Prop_Send, "m_vecMaxs", vMax);
  return vMax[2] < 45.0 && !(GetClientButtons(client) & IN_DUCK);
} 

stock bool IsValidClient(int client)
{
	if(client <= 0)
		return false;
		
	if(client > MaxClients)
		return false;
	
	if(!IsClientConnected(client))
		return false;
	
	if (IsFakeClient(client))
		return false;
	
	return IsClientInGame(client);
}